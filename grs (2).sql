-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 08, 2017 at 07:10 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `grs`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `admin_staff_id` int(10) NOT NULL,
  `admin_nid` varchar(30) NOT NULL,
  `admin_fname` varchar(30) NOT NULL,
  `admin_lname` varchar(30) NOT NULL,
  `admin_other_names` varchar(50) NOT NULL,
  `admin_username` varchar(30) NOT NULL,
  `admin_phone` varchar(30) NOT NULL,
  `admin_email` varchar(100) NOT NULL,
  `admin_date_registered` date NOT NULL,
  `admin_active_status` tinyint(1) NOT NULL DEFAULT '1',
  `admin_reason_inactive` varchar(100) DEFAULT NULL,
  `admin_password` varchar(200) NOT NULL,
  `admin_date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`admin_staff_id`, `admin_nid`, `admin_fname`, `admin_lname`, `admin_other_names`, `admin_username`, `admin_phone`, `admin_email`, `admin_date_registered`, `admin_active_status`, `admin_reason_inactive`, `admin_password`, `admin_date_updated`) VALUES
(78581, '31537790', 'Stephen', 'Mokoro', 'Nyang\'wono', 'smokoro', '0719508386', 'stephenmokoro@gmail.com', '2017-09-20', 1, NULL, 'c1dddd13e1614b504d04ba97c1236f9a', '2017-09-19 21:46:38'),
(95694, '34639901', 'Irene', 'Katoto', '', 'ikatoto', '0700153355', 'ikatoto@strathmore.edu', '2017-10-09', 1, NULL, 'c1dddd13e1614b504d04ba97c1236f9a', '2017-10-08 12:47:25');

-- --------------------------------------------------------

--
-- Table structure for table `buildings`
--

CREATE TABLE `buildings` (
  `building_id` int(5) NOT NULL,
  `building_name` varchar(100) NOT NULL,
  `build_phase_id` int(5) NOT NULL,
  `floors` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `buildings`
--

INSERT INTO `buildings` (`building_id`, `building_name`, `build_phase_id`, `floors`) VALUES
(1, 'Administration Block', 1, 1),
(2, 'Student Center', 2, 5),
(3, 'Sir Thomas More', 2, 6),
(4, 'Library', 2, 2),
(5, 'MSB', 2, 4),
(6, 'SLS', 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `conferences`
--

CREATE TABLE `conferences` (
  `conf_auto_id` int(10) NOT NULL,
  `conf_name` varchar(100) NOT NULL,
  `conf_description` varchar(200) NOT NULL,
  `conf_venue` varchar(100) NOT NULL,
  `conf_date_from` date NOT NULL,
  `conf_date_to` date NOT NULL,
  `conf_last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `conf_cancel` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `conferences`
--

INSERT INTO `conferences` (`conf_auto_id`, `conf_name`, `conf_description`, `conf_venue`, `conf_date_from`, `conf_date_to`, `conf_last_update`, `conf_cancel`) VALUES
(1, 'ICT Conference', 'FIT Conference', 'Auditorium', '2017-10-04', '2017-10-05', '2017-10-05 10:59:05', 0),
(2, 'Mathematics Conference', 'SIMS Conference', 'Auditorium', '2017-10-02', '2017-10-02', '2017-10-05 10:59:09', 0),
(3, 'Sigourney Hickman', 'Vero eum quos iure quo laudantium ipsam ea quam praesentium ut occaecat quos eaque eum cum', 'Labore impedit harum esse aut nostrum eiusmod dolore magnam non possimus adipisicing aspernatur cons', '2017-10-04', '2017-10-05', '2017-10-05 10:59:12', 0),
(4, 'Wallace Rogers', 'Nisi est officiis mollitia voluptatem Voluptatem Commodo illo', 'Dolorem quam officiis ex et culpa exercitationem eligendi consequatur Et', '2017-10-06', '2017-10-07', '2017-10-05 10:59:15', 0),
(5, 'Math Conference', '', 'Main Auditorium', '2017-10-05', '2017-10-05', '2017-10-05 10:59:19', 0);

-- --------------------------------------------------------

--
-- Table structure for table `entry_permits`
--

CREATE TABLE `entry_permits` (
  `entry_auto_id` int(10) NOT NULL,
  `entry_guest_auto_id` int(10) NOT NULL,
  `entry_date` date NOT NULL,
  `entry_time` time NOT NULL,
  `entry_expected_time_out` time DEFAULT NULL,
  `entry_actual_time_out` time DEFAULT NULL,
  `entry_card_no` varchar(10) NOT NULL,
  `entry_guest_type` int(2) NOT NULL,
  `entry_stratizen_to_visit` int(5) DEFAULT NULL,
  `entry_office_to_visit` varchar(100) DEFAULT NULL,
  `entry_visit_type` int(2) NOT NULL,
  `no_of_minors` int(5) NOT NULL,
  `entry_guest_release` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `entry_permits`
--

INSERT INTO `entry_permits` (`entry_auto_id`, `entry_guest_auto_id`, `entry_date`, `entry_time`, `entry_expected_time_out`, `entry_actual_time_out`, `entry_card_no`, `entry_guest_type`, `entry_stratizen_to_visit`, `entry_office_to_visit`, `entry_visit_type`, `no_of_minors`, `entry_guest_release`) VALUES
(1, 1, '2017-10-08', '03:41:47', '00:00:00', NULL, '', 0, NULL, 'Moses-iBiz Africa', 1, 0, 0),
(2, 2, '2017-10-08', '04:51:38', '00:00:00', NULL, '', 0, NULL, 'Peter-iLab', 1, 0, 0),
(3, 3, '2017-10-08', '04:52:46', '00:00:00', NULL, '', 0, NULL, 'Mwangi Martin - Student', 1, 0, 0),
(4, 4, '2017-10-08', '04:58:36', '00:00:00', NULL, '', 0, NULL, 'CTM', 1, 0, 0),
(5, 6, '2017-10-08', '05:06:55', '00:00:00', NULL, '', 0, NULL, 'iBiz Africa', 1, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `fbo_membership`
--

CREATE TABLE `fbo_membership` (
  `FBOM_Auto_ID` int(10) NOT NULL,
  `FBOM_FBO_ID` int(10) NOT NULL,
  `FBOM_OM_ID` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fbo_membership`
--

INSERT INTO `fbo_membership` (`FBOM_Auto_ID`, `FBOM_FBO_ID`, `FBOM_OM_ID`) VALUES
(1, 2, 34639901),
(2, 5, 34639901),
(3, 4, 34639901);

-- --------------------------------------------------------

--
-- Table structure for table `floors`
--

CREATE TABLE `floors` (
  `floor_no` int(5) NOT NULL,
  `floor_name` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `floors`
--

INSERT INTO `floors` (`floor_no`, `floor_name`) VALUES
(0, 'Basement'),
(1, 'Ground Floor'),
(2, '1st Floor'),
(3, '2nd Floor'),
(4, '3rd Floor'),
(5, '4th Floor'),
(6, '5th Floor'),
(7, '6th Floor');

-- --------------------------------------------------------

--
-- Table structure for table `guards`
--

CREATE TABLE `guards` (
  `guard_phase_id` int(5) NOT NULL,
  `guard_staff_id` int(10) NOT NULL,
  `guard_nid` varchar(30) NOT NULL,
  `guard_fname` varchar(30) NOT NULL,
  `guard_lname` varchar(30) NOT NULL,
  `guard_other_names` varchar(50) NOT NULL,
  `guard_username` varchar(30) NOT NULL,
  `guard_phone` varchar(30) NOT NULL,
  `guard_email` varchar(100) NOT NULL,
  `guard_date_registered` date NOT NULL,
  `guard_active_status` tinyint(1) NOT NULL DEFAULT '1',
  `guard_reason_inactive` varchar(100) DEFAULT NULL,
  `guard_password` varchar(200) NOT NULL,
  `guard_date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `guards`
--

INSERT INTO `guards` (`guard_phase_id`, `guard_staff_id`, `guard_nid`, `guard_fname`, `guard_lname`, `guard_other_names`, `guard_username`, `guard_phone`, `guard_email`, `guard_date_registered`, `guard_active_status`, `guard_reason_inactive`, `guard_password`, `guard_date_updated`) VALUES
(2, 78581, '31537790', 'Stephen', 'Mokoro', '', 'mokoros', '0719508386', 'smokoro@gmail.com', '2017-10-08', 1, NULL, '827ccb0eea8a706c4c34a16891f84e7b', '2017-10-08 12:58:05');

-- --------------------------------------------------------

--
-- Table structure for table `guests`
--

CREATE TABLE `guests` (
  `guest_auto_id` int(10) NOT NULL,
  `guest_id` varchar(30) NOT NULL,
  `guest_id_type` int(5) NOT NULL,
  `guest_fname` varchar(30) NOT NULL,
  `guest_lname` varchar(30) NOT NULL,
  `guest_other_names` varchar(50) NOT NULL,
  `guest_phone` varchar(30) DEFAULT NULL,
  `id_upload` tinyint(1) DEFAULT '0',
  `date_registered` date NOT NULL,
  `active_status` tinyint(1) NOT NULL DEFAULT '1',
  `reason_inactive` varchar(100) DEFAULT NULL,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `guests`
--

INSERT INTO `guests` (`guest_auto_id`, `guest_id`, `guest_id_type`, `guest_fname`, `guest_lname`, `guest_other_names`, `guest_phone`, `id_upload`, `date_registered`, `active_status`, `reason_inactive`, `date_updated`) VALUES
(1, '4413732', 1, 'Jesse', 'Kirowo', 'M.O', '0722771108', 0, '2017-10-04', 1, NULL, '2017-10-08 13:01:06'),
(2, '27815348', 1, 'Jedidah', 'Wanjeru', 'Theuri', '0710247276', 0, '2017-10-04', 1, NULL, '2017-10-08 13:02:13'),
(3, '33143520', 1, 'Eric ', 'Chege', 'Muraya', '0715887002', 0, '2017-10-04', 1, NULL, '2017-10-08 13:03:33'),
(4, '22818729', 1, 'Christopher', 'Abuta', 'Ondieki', '0724111718', 0, '2017-10-04', 1, NULL, '2017-10-08 13:03:33'),
(5, '29919817', 1, 'Jacqueline ', 'Adhiambo', '', '0704452426', 0, '2017-10-04', 1, NULL, '2017-10-08 14:06:07'),
(6, '27897607', 1, 'Daniel', 'Osakaise', 'Ochudi', '0720020766', 0, '2017-10-04', 1, NULL, '2017-10-08 13:04:57'),
(7, '27381941', 1, 'Malaki', 'Kamau', 'Kinyanjui', 'ID: 27381941', 0, '2017-10-04', 1, NULL, '2017-10-08 13:16:26'),
(16, '2880032', 1, 'Lilian', 'Mawia', 'Jane', 'ID: 2880032', 0, '2017-10-04', 1, NULL, '2017-10-08 13:18:59'),
(17, '33841428', 1, 'Davis', 'Kiprono', 'Kosgei', '0720915622', 0, '2017-10-05', 1, NULL, '2017-10-08 13:20:35'),
(18, '12603071', 1, 'George ', 'Morara', '', '0722245347', 0, '2017-10-04', 1, NULL, '2017-10-08 13:21:08'),
(19, '3496392', 1, 'Stephen', 'Mwangi', '', '0722577480', 0, '2017-10-04', 1, NULL, '2017-10-08 13:21:43'),
(20, '225361379', 1, 'Willy ', 'Kimenyi', '', '0722302555', 0, '2017-10-05', 1, NULL, '2017-10-08 13:22:17'),
(21, '700071664', 1, 'Mary', 'Nereah', '', '0719536941', 0, '2017-10-05', 1, NULL, '2017-10-08 13:23:08'),
(22, '208655813', 1, 'Laura', 'Fiona', 'Akinyi', '0723514206', 0, '2017-10-05', 1, NULL, '2017-10-08 13:23:49'),
(23, '2029507', 1, 'Lucy', 'Wangari', 'Muguiyi', '0727616313', 0, '2017-10-05', 1, NULL, '2017-10-08 13:24:30'),
(24, '22262990', 1, 'Dorcas', 'Mwelu', '', '0723216776', 0, '2017-10-05', 1, NULL, '2017-10-08 13:25:34'),
(25, '22820058', 1, 'Moses', 'Kariuki', 'Njeri', '0722415542', 0, '2017-10-05', 1, NULL, '2017-10-08 13:26:18'),
(26, '9541853', 1, 'Flora', 'Wanja', 'Ngiai', '0727631070', 0, '2017-10-05', 1, NULL, '2017-10-08 13:29:01'),
(27, '25727837', 1, 'Bendicta', 'Sangaro', 'Ambani', '0729092490', 0, '2017-10-05', 1, NULL, '2017-10-08 13:29:43'),
(28, '31678871', 1, 'Morris', 'Ngei', 'Kavisti', '0700842369', 0, '2017-10-05', 1, NULL, '2017-10-08 13:30:54');

-- --------------------------------------------------------

--
-- Table structure for table `hd_staff`
--

CREATE TABLE `hd_staff` (
  `hd_staff_auto_id` int(10) NOT NULL,
  `hd_staff_staff_id` int(10) NOT NULL,
  `hd_staff_nid` varchar(30) NOT NULL,
  `hd_staff_fname` varchar(30) NOT NULL,
  `hd_staff_lname` varchar(30) NOT NULL,
  `hd_staff_other_names` varchar(50) NOT NULL,
  `hd_staff_username` varchar(30) NOT NULL,
  `hd_staff_phone` varchar(30) NOT NULL,
  `hd_staff_email` varchar(100) NOT NULL,
  `staff_date_registered` date NOT NULL,
  `hd_staff_active_status` tinyint(1) NOT NULL DEFAULT '1',
  `hd_staff_reason_inactive` varchar(100) DEFAULT NULL,
  `hd_staff_password` varchar(200) NOT NULL,
  `hd_staff_date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `hd_staff`
--

INSERT INTO `hd_staff` (`hd_staff_auto_id`, `hd_staff_staff_id`, `hd_staff_nid`, `hd_staff_fname`, `hd_staff_lname`, `hd_staff_other_names`, `hd_staff_username`, `hd_staff_phone`, `hd_staff_email`, `staff_date_registered`, `hd_staff_active_status`, `hd_staff_reason_inactive`, `hd_staff_password`, `hd_staff_date_updated`) VALUES
(5, 90100, '32143243', 'Esther', 'Mutua', '', 'smutua', '0719508387', 'smutua@gmail.com', '2017-09-20', 1, NULL, 'c1dddd13e1614b504d04ba97c1236f9a', '2017-09-19 23:04:45');

-- --------------------------------------------------------

--
-- Table structure for table `helpdesks`
--

CREATE TABLE `helpdesks` (
  `hd_auto_id` int(5) NOT NULL,
  `hd_name` varchar(150) NOT NULL,
  `hd_phase_id` int(5) NOT NULL,
  `hd_building_id` int(5) DEFAULT NULL,
  `hd_floor_no` int(5) DEFAULT NULL,
  `date_registered` date NOT NULL,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `helpdesks`
--

INSERT INTO `helpdesks` (`hd_auto_id`, `hd_name`, `hd_phase_id`, `hd_building_id`, `hd_floor_no`, `date_registered`, `date_updated`) VALUES
(5, 'FIT Helpdesk', 1, 1, 2, '2017-09-20', '2017-09-19 22:57:33'),
(6, 'iLab Africa Helpdesk', 2, 2, 5, '2017-09-20', '2017-09-19 22:58:13'),
(7, 'iBiz Africa Helpdesk', 2, 2, 6, '2017-09-20', '2017-09-19 22:58:35'),
(8, 'SMC', 2, 3, 4, '2017-09-20', '2017-09-19 22:59:11'),
(9, 'SOA Helpdesk', 1, 1, 1, '2017-09-20', '2017-09-19 22:59:34'),
(10, 'SIMS Helpdesk', 1, 1, 1, '2017-09-20', '2017-09-19 22:59:59');

-- --------------------------------------------------------

--
-- Table structure for table `id_types`
--

CREATE TABLE `id_types` (
  `id_no` int(5) NOT NULL,
  `id_type` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `id_types`
--

INSERT INTO `id_types` (`id_no`, `id_type`) VALUES
(1, 'National ID'),
(2, 'Passport'),
(3, 'Military ID'),
(4, 'Staff ID'),
(5, 'Student ID'),
(6, 'Other');

-- --------------------------------------------------------

--
-- Table structure for table `offices`
--

CREATE TABLE `offices` (
  `office_auto_id` int(5) NOT NULL,
  `office_name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `offices`
--

INSERT INTO `offices` (`office_auto_id`, `office_name`) VALUES
(1, 'Admissions Office'),
(2, 'Cafeteria'),
(3, 'Office of the DoS'),
(4, 'Clubs Office'),
(5, 'Students Council Office'),
(6, 'Communications Office');

-- --------------------------------------------------------

--
-- Table structure for table `phases`
--

CREATE TABLE `phases` (
  `phase_id` int(5) NOT NULL,
  `phase_name` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `phases`
--

INSERT INTO `phases` (`phase_id`, `phase_name`) VALUES
(1, 'Phase I'),
(2, 'Phase II');

-- --------------------------------------------------------

--
-- Table structure for table `stratizens`
--

CREATE TABLE `stratizens` (
  `stratizen_auto_id` int(10) NOT NULL,
  `stratizen_su_id` int(10) NOT NULL,
  `stratizen_type_id` int(5) NOT NULL,
  `stratizen_nid` varchar(30) NOT NULL,
  `stratizen_fname` varchar(30) NOT NULL,
  `stratizen_lname` varchar(30) NOT NULL,
  `stratizen_other_names` varchar(50) NOT NULL,
  `stratizen_extension` int(10) NOT NULL,
  `stratizen_phone` varchar(30) NOT NULL,
  `stratizen_email` varchar(100) NOT NULL,
  `stratizen_staffroom` varchar(50) DEFAULT NULL,
  `date_registered` date NOT NULL,
  `stratizen_active_status` tinyint(1) NOT NULL DEFAULT '1',
  `stratizen_reason_inactive` varchar(100) DEFAULT NULL,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `stratizens`
--

INSERT INTO `stratizens` (`stratizen_auto_id`, `stratizen_su_id`, `stratizen_type_id`, `stratizen_nid`, `stratizen_fname`, `stratizen_lname`, `stratizen_other_names`, `stratizen_extension`, `stratizen_phone`, `stratizen_email`, `stratizen_staffroom`, `date_registered`, `stratizen_active_status`, `stratizen_reason_inactive`, `date_updated`) VALUES
(4, 78582, 2, '31537791', 'Sample', 'Makali', 'Malena', 0, '0719508389', 'sampo@sampo.com', 'Fadhila Staffroom', '2017-09-20', 1, NULL, '2017-09-20 00:58:25'),
(5, 75633, 2, '31515352', 'Macro', 'Main', 'Katadro', 0, '729508386', 'katadro@gmail.com', 'Jasiri', '2017-09-20', 1, NULL, '2017-09-20 03:32:04'),
(6, 42553, 2, '544', 'Maembe', 'Yatoto', 'Makanane', 0, '0719506386', 'tatoto@gmail.com', 'Jasiri', '2017-09-20', 1, NULL, '2017-09-20 03:57:11'),
(7, 95694, 1, '35458964', 'Irene', 'Kanario', 'Nkondi', 414, '0700153356', 'ikanario5@gmail.com', NULL, '2017-10-03', 1, NULL, '2017-10-03 16:36:35');

-- --------------------------------------------------------

--
-- Table structure for table `stratizen_types`
--

CREATE TABLE `stratizen_types` (
  `stratizen_type_id` int(5) NOT NULL,
  `stratizen_type_name` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stratizen_types`
--

INSERT INTO `stratizen_types` (`stratizen_type_id`, `stratizen_type_name`) VALUES
(1, 'Student'),
(2, 'Staff');

-- --------------------------------------------------------

--
-- Table structure for table `visit_types`
--

CREATE TABLE `visit_types` (
  `visit_type_auto_id` int(11) NOT NULL,
  `visit_type_name` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `visit_types`
--

INSERT INTO `visit_types` (`visit_type_auto_id`, `visit_type_name`) VALUES
(1, 'Official'),
(2, 'Personal'),
(3, 'Other');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`admin_staff_id`),
  ADD UNIQUE KEY `admin_nid` (`admin_nid`),
  ADD UNIQUE KEY `admin_phone` (`admin_phone`),
  ADD UNIQUE KEY `admin_email` (`admin_email`),
  ADD UNIQUE KEY `admin_username` (`admin_username`);

--
-- Indexes for table `buildings`
--
ALTER TABLE `buildings`
  ADD PRIMARY KEY (`building_id`),
  ADD KEY `build_phase_id_fk` (`build_phase_id`);

--
-- Indexes for table `conferences`
--
ALTER TABLE `conferences`
  ADD PRIMARY KEY (`conf_auto_id`);

--
-- Indexes for table `entry_permits`
--
ALTER TABLE `entry_permits`
  ADD PRIMARY KEY (`entry_auto_id`),
  ADD KEY `entry_guest_auto_id_fk` (`entry_guest_auto_id`),
  ADD KEY `entry_guest_type_fk` (`entry_guest_type`),
  ADD KEY `entry_visit_type_fk` (`entry_visit_type`),
  ADD KEY `entry_stratizen_to_visit_fk` (`entry_stratizen_to_visit`);

--
-- Indexes for table `fbo_membership`
--
ALTER TABLE `fbo_membership`
  ADD PRIMARY KEY (`FBOM_Auto_ID`);

--
-- Indexes for table `floors`
--
ALTER TABLE `floors`
  ADD PRIMARY KEY (`floor_no`);

--
-- Indexes for table `guards`
--
ALTER TABLE `guards`
  ADD PRIMARY KEY (`guard_staff_id`),
  ADD UNIQUE KEY `officer_nid` (`guard_nid`),
  ADD UNIQUE KEY `officer_phone` (`guard_phone`),
  ADD UNIQUE KEY `officer_email` (`guard_email`),
  ADD UNIQUE KEY `officer_username` (`guard_username`),
  ADD KEY `officer_phase_id_fk` (`guard_phase_id`);

--
-- Indexes for table `guests`
--
ALTER TABLE `guests`
  ADD PRIMARY KEY (`guest_auto_id`),
  ADD UNIQUE KEY `guest_id` (`guest_id`),
  ADD UNIQUE KEY `guest_phone` (`guest_phone`),
  ADD KEY `guest_id_type_fk` (`guest_id_type`);

--
-- Indexes for table `hd_staff`
--
ALTER TABLE `hd_staff`
  ADD PRIMARY KEY (`hd_staff_staff_id`),
  ADD UNIQUE KEY `hd_officer_nid` (`hd_staff_nid`),
  ADD UNIQUE KEY `hd_officer_phone` (`hd_staff_phone`),
  ADD UNIQUE KEY `hd_officer_email` (`hd_staff_email`),
  ADD UNIQUE KEY `hd_officer_username` (`hd_staff_username`),
  ADD KEY `hd_officer_phase_id_fk` (`hd_staff_auto_id`);

--
-- Indexes for table `helpdesks`
--
ALTER TABLE `helpdesks`
  ADD PRIMARY KEY (`hd_auto_id`),
  ADD KEY `hd_phase_id_fk` (`hd_phase_id`),
  ADD KEY `hd_floor_no_fk` (`hd_floor_no`),
  ADD KEY `hd_building_id_fk` (`hd_building_id`);

--
-- Indexes for table `id_types`
--
ALTER TABLE `id_types`
  ADD PRIMARY KEY (`id_no`);

--
-- Indexes for table `offices`
--
ALTER TABLE `offices`
  ADD PRIMARY KEY (`office_auto_id`);

--
-- Indexes for table `phases`
--
ALTER TABLE `phases`
  ADD PRIMARY KEY (`phase_id`),
  ADD UNIQUE KEY `phase_name` (`phase_name`);

--
-- Indexes for table `stratizens`
--
ALTER TABLE `stratizens`
  ADD PRIMARY KEY (`stratizen_auto_id`),
  ADD UNIQUE KEY `stratizen_su_id` (`stratizen_su_id`,`stratizen_nid`,`stratizen_phone`,`stratizen_email`),
  ADD KEY `stratizen_type_fk` (`stratizen_type_id`);

--
-- Indexes for table `stratizen_types`
--
ALTER TABLE `stratizen_types`
  ADD PRIMARY KEY (`stratizen_type_id`);

--
-- Indexes for table `visit_types`
--
ALTER TABLE `visit_types`
  ADD PRIMARY KEY (`visit_type_auto_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `buildings`
--
ALTER TABLE `buildings`
  MODIFY `building_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `conferences`
--
ALTER TABLE `conferences`
  MODIFY `conf_auto_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `entry_permits`
--
ALTER TABLE `entry_permits`
  MODIFY `entry_auto_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `fbo_membership`
--
ALTER TABLE `fbo_membership`
  MODIFY `FBOM_Auto_ID` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `guests`
--
ALTER TABLE `guests`
  MODIFY `guest_auto_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `helpdesks`
--
ALTER TABLE `helpdesks`
  MODIFY `hd_auto_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `offices`
--
ALTER TABLE `offices`
  MODIFY `office_auto_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `phases`
--
ALTER TABLE `phases`
  MODIFY `phase_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `stratizens`
--
ALTER TABLE `stratizens`
  MODIFY `stratizen_auto_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `visit_types`
--
ALTER TABLE `visit_types`
  MODIFY `visit_type_auto_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `buildings`
--
ALTER TABLE `buildings`
  ADD CONSTRAINT `build_phase_id_fk` FOREIGN KEY (`build_phase_id`) REFERENCES `phases` (`phase_id`);

--
-- Constraints for table `entry_permits`
--
ALTER TABLE `entry_permits`
  ADD CONSTRAINT `entry_guest_auto_id_fk` FOREIGN KEY (`entry_guest_auto_id`) REFERENCES `guests` (`guest_auto_id`),
  ADD CONSTRAINT `entry_stratizen_to_visit_fk` FOREIGN KEY (`entry_stratizen_to_visit`) REFERENCES `stratizens` (`stratizen_auto_id`),
  ADD CONSTRAINT `entry_visit_type_fk` FOREIGN KEY (`entry_visit_type`) REFERENCES `visit_types` (`visit_type_auto_id`);

--
-- Constraints for table `guards`
--
ALTER TABLE `guards`
  ADD CONSTRAINT `guard_phase_id_fk` FOREIGN KEY (`guard_phase_id`) REFERENCES `phases` (`phase_id`);

--
-- Constraints for table `guests`
--
ALTER TABLE `guests`
  ADD CONSTRAINT `guest_id_type_fk` FOREIGN KEY (`guest_id_type`) REFERENCES `id_types` (`id_no`);

--
-- Constraints for table `hd_staff`
--
ALTER TABLE `hd_staff`
  ADD CONSTRAINT `hd_staff_auto_id_fk` FOREIGN KEY (`hd_staff_auto_id`) REFERENCES `helpdesks` (`hd_auto_id`);

--
-- Constraints for table `helpdesks`
--
ALTER TABLE `helpdesks`
  ADD CONSTRAINT `hd_building_id_fk` FOREIGN KEY (`hd_building_id`) REFERENCES `buildings` (`building_id`),
  ADD CONSTRAINT `hd_floor_no_fk` FOREIGN KEY (`hd_floor_no`) REFERENCES `floors` (`floor_no`),
  ADD CONSTRAINT `hd_phase_id_fk` FOREIGN KEY (`hd_phase_id`) REFERENCES `phases` (`phase_id`);

--
-- Constraints for table `stratizens`
--
ALTER TABLE `stratizens`
  ADD CONSTRAINT `stratizen_type_fk` FOREIGN KEY (`stratizen_type_id`) REFERENCES `stratizen_types` (`stratizen_type_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
