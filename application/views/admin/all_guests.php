<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SGS - All Guests</title>
    
   <?php $this->load->view('headerlinks/headerlinks.php'); ?>
   <link href="<?php echo base_url(); ?>assets/css/core.css" rel="stylesheet" type="text/css" />
   <link href="<?php echo base_url(); ?>assets/general-css/smsgeneral.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div id="wrapper">
    <?php $this->load->view('admin/adminnav.php'); ?><!--navigation -->
    <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h4 class="page-header" style="margin-top:10px;color:darkgrey"> All Guests as of <?php echo date("D M d, Y");?> </h4>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            
             <span data-placement="top" data-toggle="tooltip" title="Refresh"><button class="btn btn-s" data-title="Refresh "  id="refresh" ><span class="fa fa-refresh"></span>&nbsp;Refresh</button>
            </span>
            <span data-placement="top" data-toggle="tooltip" title="Print All">
                    <a class="btn btn-s" data-title="Print All" type="button" href="<?php echo base_url('');?>"><span class="fa fa-print"></span>&nbsp;Print All</a>
            </span>
            <div class="row">
                <div class="col-md-12">
                    <table  class="table table-striped table-bordered table-hover display responsive nowrap" cellspacing="0" width="100%" id="allguests"  >
                        <thead>
                            <tr>
                                <th class="text-center">Full Name</th>
                                <th class="text-center">Id No</th>
                                <th class="text-center">Id Type</th>
                                <th class="text-center">Phone</th>
                                <th class="text-center"></th>
                             </tr>
                        </thead>
                        <tbody >
                            <?php 
                            foreach($guests as $guest){ 
                               ?>
                            <tr>
                                <td class="text-left"><?php  echo $guest['guest_fname']. " ".$guest['guest_lname']; ?></td>
                                <td class="text-center"><?php  echo $guest['guest_id'];  ?></td>
                                <td class="text-center"><?php  echo $guest['id_type'];  ?></td>
                                <td class="text-center"><?php  echo $guest['guest_phone']; ?></td>
                                <td class="text-center">
                                      <?php $status=$guest['id_upload']; if($status==FALSE)
                                        { echo' <form style="display:inline;" name='; echo '"formViewGuest_'. $guest['guest_auto_id'].'"';  echo 'method="post" action="'; echo base_url('MC/viewGuest');echo '">
                                        <div class="form-group col-md-12 col-lg-12" style="display:none">
                                            <label for="guestId" class="control-label">Guest PID*</label>
                                            <input required="required" class="form-control" name="guestId" id="guestId" placeholder="101" value="'; echo $guest['guest_auto_id'];echo '">
                                        </div>
                                        <span data-placement="top" data-toggle="tooltip" title="View Guest">
                                        <button class="btn btn-default btn-s" data-title="View Guest" id='; echo '"viewGuest_'. $guest['guest_auto_id'].'"';  echo ' name='; echo '"viewGuest_'. $guest['guest_auto_id'].'"'; echo 'type="submit" ><span class="fa fa-eye-slash" style="color:red"></span>&nbsp;View Guest</button>
                                        </span>
                                    </form> ';
                                        }else {
                                                echo' <form style="display:inline;" name='; echo '"formViewGuest_'. $guest['guest_auto_id'].'"';  echo 'method="post" action="'; echo base_url('MC/viewGuest');echo '">
                                        <div class="form-group col-md-12 col-lg-12" style="display:none">
                                            <label for="guestId" class="control-label">Guest PID*</label>
                                            <input required="required" class="form-control" name="guestId" id="guestId" placeholder="101" value="'; echo $guest['guest_auto_id'];echo '">
                                        </div>
                                        <span data-placement="top" data-toggle="tooltip" title="View Guest">
                                        <button class="btn btn-default btn-s" data-title="View Guest" id='; echo '"viewGuest_'. $guest['guest_auto_id'].'"';  echo ' name='; echo '"viewGuest_'. $guest['guest_auto_id'].'"'; echo 'type="submit" ><span class="fa fa-eye" style="color:darkblue"></span>&nbsp;View Guest</button>
                                        </span>
                                    </form> ';}?>

                                </td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                    <!-- /.table-responsive -->
                </div>
            </div>
           
            <div class="modal fade" id="disable_guest" tabindex="-1" role="dialog" aria-labelledby="Disable" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-body ">
                            <button class="btn btn-danger" style="width: 100%!important;"><span class="pull-left"><span class="fa fa-warning"></span> Are you sure you want to disable this guest?</span></button>
                            <form role="form" method="post"  id="guest_disable" action="<?php echo base_url(); ?>MC/disableguest">
                                <div class="form-group col-md-12 col-lg-12" hidden="true">
                                    <label for="staffId" class="control-label">guest Id</label>
                                    <input type="text" name="staffId" placeholder="Staff Id" class=" form-control" id="staffId" required="required" readonly="true" >
                                </div>
                                <br><br>
                                <div class="form-group col-md-12 col-lg-12 " >
                                    <label for="reason_disable" class="control-label">Reason to disable guest*</label>
                                    <textarea required="required" class="form-control" name="reasonToDisable" placeholder="e.g. Transfer" ></textarea>
                                </div>
                                <button type="submit" class="btn btn-warning" id="disable_confirm"><span class="fa fa-check"></span> PROCEED</button>
                                <button type="button" class="btn " data-dismiss="modal"><span class="fa fa-remove"></span> Exit</button>
                            </form>
                        </div>
                    </div>
                    <!-- /.modal-content --> 
                </div>
                <!-- /.modal-dialog --> 
            </div>
    </div>
    <!-- /#page-wrapper -->
</div>
<!-- /#wrapper -->
<?php $this->load->view('scriptlinks/scriptlinks.php'); ?>
<script>
$(document).ready(function () {
    //datatable initialization
     var table=$('#allguests').DataTable({responsive:true,"iDisplayLength": 5,"lengthMenu": [[5, 25, 50, 100, 200, -1], [5, 25, 50, 100, 200, "All"]],columnDefs: [ { orderable: false, targets: [4] }], "aaSorting": []
   });
});
//to refresh the page
$( "#refresh").click( function(event)
    {
        window.setTimeout(function(){location.reload()},1)

    });
</script>
</body>
</html>
