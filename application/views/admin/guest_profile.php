<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SGS - Guest Profile</title>
    
   <?php $this->load->view('headerlinks/headerlinks.php'); ?>
   <link href="<?php echo base_url(); ?>assets/css/core.css" rel="stylesheet" type="text/css" />
   <link href="<?php echo base_url(); ?>assets/general-css/smsgeneral.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div id="wrapper">
    <?php $this->load->view('admin/officernav.php'); ?><!--navigation -->
    <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h4 class="page-header" style="margin-top:10px;color:darkgrey"> <?php echo date("D M d, Y");?> Guests</h4>
                </div>
                <!-- /.col-lg-12 -->
            </div>
                <div class="row">
                     <?php foreach ($guest as $gst){?>
                    <div class="col-md-12">
                        <div class="col-md-3" style="text-align: center;margin-right: auto">
                            <div class="col-md-12" style="display: inline-block;text-align: center">
                                <img src="<?php echo base_url(); ?>assets/images/placeholder_primary_id.jpg"
                            alt="" class="img-rounded img-responsive" />
                                <br>
                            </div>
                        </div>
                        <div class="col-md-9">
                             <blockquote>
                                <p style="color: darkblue"><strong><?php echo $gst['guest_fname']." ".$gst['guest_lname']. " ".$gst['guest_other_names'];?></strong></p> 
                                <!-- <small><cite title="Source Title">  <i class="glyphicon glyphicon-map-marker"></i></cite></small> -->
                                 <p> <i class="fa fa-mobile-phone fa-1x"></i> <?php echo $gst['guest_phone'];?></p>
                                 <p><i class="fa fa-id-card-o"></i> <?php echo $gst['guest_id'];?> </p>
                            </blockquote>
                        </div>
                    </div>
                    <?php }?>
                </div>
    </div>
    <!-- /#page-wrapper -->
</div>
<!-- /#wrapper -->
<?php $this->load->view('scriptlinks/scriptlinks.php'); ?>
<script>
//to refresh the page
$( "#refresh").click( function(event)
    {
        window.setTimeout(function(){location.reload()},1)

    });
</script>
</body>
</html>
