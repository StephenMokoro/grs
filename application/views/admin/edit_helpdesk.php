<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>SGS - Helpdesk Registration</title>
   <?php $this->load->view('headerlinks/headerlinks.php'); ?>
   <link href="<?php echo base_url(); ?>assets/css/core.css" rel="stylesheet" type="text/css" />
   <link href="<?php echo base_url(); ?>assets/general-css/smsgeneral.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div id="wrapper">
    <?php $this->load->view('admin/officernav.php');?><!--navigation -->
    <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h4 class="page-header" style="margin-top:10px;color:darkgrey">Edit Helpdesk </h4>
                </div>
                <!-- /.col-lg-12 -->
            </div>
                <?php $msg = $this->session->flashdata('msg');
                $successful= $msg['success']; $failed=  $msg['error']; if ($successful=="" && $failed!=""){ echo '
                <div class="messagebox alert alert-danger" style="display: block">
                        <button type="button" class="close" data-dismiss="alert">*</button>
                        <div class="cs-text">
                            <i class="fa fa-close"></i>
                            <strong><span>';echo $msg['error']; echo '</span></strong>
                        </div> 
                </div>';}else if($successful=="" && $failed==""){echo '<div></div>';} else if ($successful!="" && $failed==""){ echo '
                <div class="messagebox alert alert-success" style="display: block">
                        <button type="button" class="close" data-dismiss="alert">*</button>
                        <div class="cs-text">
                            <i class="fa fa-check-circle-o"></i>
                            <strong><span>';echo $msg['success'];echo '</span></strong>
                        </div> 
                </div>';}?>
                <div class="modal-body">
                    <?php foreach ($helpdesk as $hdesk) {
                     ?>
                    <form role="form" id="helpdesk_registration" method="post" action="<?php echo base_url(); ?>MC/updateHelpdesk">
                        <div class="row setup-content" >
                            <div class="col-xs-12">
                                <div class="col-md-12">
                                    <div class="form-group col-md-6 col-lg-6 " hidden="true">
                                        <label for="helpdeskId" class="control-label"> Helpdesk Id*</label>
                                        <input type="text" name="helpdeskId" placeholder="e.g. 101" class="form-control" id="helpdeskId" required="required" value=<?php echo '"'.$hdesk['hd_auto_id'].'"';   ?> readonly="true">
                                    </div>
                                     <div class="form-group col-md-6 col-lg-6 ">
                                        <label for="helpdeskName" class="control-label">Helpdesk Name*</label>
                                        <input type="text" name="helpdeskName" placeholder="e.g. iLab Africa" class="form-control" id="helpdeskName" required="required" value=<?php echo '"'.$hdesk['hd_name'].'"';   ?>>
                                    </div>
                                    <div class="form-group col-md-6 col-lg-6" >
                                        <label for="helpdesk" class="control-label">Phase*</label>
                                        <select type="text" name="phaseId" class="form-control" id="phaseId" required="required">
                                            <option value=<?php echo '"'.$hdesk['hd_phase_id'].'"';   ?>><?php echo $hdesk['phase_name'];   ?></option>
                                            <option value="1">Phase I</option>
                                            <option value="2">Phase II</option>
                                            <option value="3">Phase III</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-6 col-lg-6" >
                                        <label for="buildingId" class="control-label">Building*</label>
                                        <select type="text" name="buildingId" class="form-control" id="buildingId" required="required">
                                            <option value=<?php echo '"'.$hdesk['hd_building_id'].'"';   ?>><?php echo $hdesk['building_name'];   ?></option>
                                            <?php  foreach($buildings as $building){?>
                                            <option value=<?php echo '"'.$building['building_id'].'"';   ?> ><?php echo $building['building_name'];   }?></option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-6 col-lg-6" >
                                        <label for="floorNo" class="control-label">Floor No</label>
                                        <select type="text" name="floorNo" class="form-control" id="floorNo" required="required">
                                            <option value=<?php echo '"'.$hdesk['hd_floor_no'].'"';   ?>><?php echo $hdesk['floor_name'];  } ?></option>
                                            <option value="0">Basement</option>
                                            <option value="1">Ground Floor</option>
                                            <option value="2">1<sup>st</sup> Floor</option>
                                            <option value="3">2<sup>nd</sup> Floor</option>
                                            <option value="4">3<sup>rd</sup> Floor</option>
                                            <option value="5">4<sup>th</sup> Floor</option>
                                            <option value="6">5<sup>th</sup> Floor</option>
                                            <option value="7">6<sup>th</sup> Floor</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-12 col-lg-12">
                                    <div class="modal-header"></div>
                                        <br>
                                        <input type="submit" class="btn btn-warning" value="Update">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
    </div>
    <!-- /#page-wrapper -->
</div>
<!-- /#wrapper -->
<?php $this->load->view('scriptlinks/scriptlinks.php'); ?>
<script>
$(document).ready(function () {
    var  submitBtn = $('input[type="submit"]');
        // allWells.show();
    submitBtn.click(function(){
        var curStep = $(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            curInputs = curStep.find("input,select"),
            isValid = true;
        $(".form-group").removeClass("has-error");
        for(var i=0; i<curInputs.length; i++){
            if (!curInputs[i].validity.valid){
                isValid = false;
                $(curInputs[i]).closest(".form-group").addClass("has-error");
            }
        }
        if (isValid)
            nextStepWizard.removeAttr('disabled').trigger('click');
    });
});
//to refresh the page
$( "#refresh").click( function(event)
    {
        window.setTimeout(function(){location.reload()},1)

    });
</script>
</body>
</html>