<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SGS - Helpdesks</title>
    
   <?php $this->load->view('headerlinks/headerlinks.php'); ?>
   <link href="<?php echo base_url(); ?>assets/css/core.css" rel="stylesheet" type="text/css" />
   <link href="<?php echo base_url(); ?>assets/general-css/smsgeneral.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div id="wrapper">
    <?php $this->load->view('admin/adminnav.php'); ?><!--navigation -->
    <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h4 class="page-header" style="margin-top:10px;color:darkgrey">Helpdesks as of <?php echo date("D M d, Y");?></h4>
                </div>
                <!-- /.col-lg-12 -->
            </div>
           
            <span data-placement="top" data-toggle="tooltip" title="Print All">
                    <a class="btn btn-s" data-title="Print All" type="button" href="<?php echo base_url('');?>"><span class="fa fa-print"></span>&nbsp;Print All</a>
            </span>

            <div class="row">
                <div class="col-md-12">
                    <table  class="table table-striped table-bordered table-hover display responsive nowrap" cellspacing="0" width="100%" id="helpdesks"  >
                        <thead>
                            <tr>
                                <th class="text-center">Helpdesk</th>
                                <th class="text-center">Phase</th>
                                <th class="text-center">Building</th>
                                <th class="text-center">Floor</th>
                             </tr>
                        </thead>
                        <tbody >
                            <?php  $count=1;
                            foreach($helpdesks as $helpdesk){ 
                               ?>
                            <tr>
                                <td class="text-left"><?php  echo $helpdesk['hd_name']; ?></td>
                                <td class="text-center"><?php  echo $helpdesk['phase_name'];  ?></td>
                                <td class="text-center"><?php  echo $helpdesk['building_name']; ?></td>
                                <td class="text-center"><?php  echo $helpdesk['floor_name']; ?></td>
                               
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                    <!-- /.table-responsive -->
                </div>
            </div>
            <div id="hd_helpdesk-registration-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="full-width-modalLabel" aria-hidden="true" style="display: none;">
                <div class="modal-dialog modal-full">
                    <div class="modal-content">
                        <div class="modal-header" style="border-bottom: none!important;margin-bottom: -20px">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                <h4 class="modal-title" id="full-width-modalLabel" ><strong style="color: darkred">Helpdesk registration</strong></h4>
                        </div>
                    </>
                    <div class="modal-body">
                        <form role="form" id="helpdesk_registration" method="post" action="<?php echo base_url(); ?>MC/newHelpdesk">
                            <div class="row setup-content" >
                                <div class="col-xs-12">
                                    <div class="col-md-12">
                                         <div class="form-group col-md-6 col-lg-6 ">
                                            <label for="helpdeskName" class="control-label">Helpdesk Name*</label>
                                            <input type="text" name="helpdeskName" placeholder="e.g. iLab Africa" class="form-control" id="helpdeskName" required="required">
                                        </div>
                                        <div class="form-group col-md-6 col-lg-6" >
                                            <label for="phaseId" class="control-label">Phase*</label>
                                            <select type="text" name="phaseId" class="form-control" id="phaseId" required="required">
                                                <option>--Select Phase--</option>
                                                <option value="1">Phase I</option>
                                                <option value="2">Phase II</option>
                                                <option value="3">Phase III</option>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-6 col-lg-6" >
                                            <label for="buildingId" class="control-label">Building*</label>
                                            <select type="text" name="buildingId" class="form-control" id="buildingId" required="required">
                                                <option>--Select Building--</option>
                                                <?php  foreach($buildings as $building){?>
                                                <option value=<?php echo '"'.$building['building_id'].'"';   ?> ><?php echo $building['building_name'];   }?></option>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-6 col-lg-6" >
                                            <label for="floorNo" class="control-label">Floor No</label>
                                            <select type="text" name="floorNo" class="form-control" id="floorNo" required="required">
                                                <option >--Select Floor--</option>
                                                <option value="0">Basement</option>
                                                <option value="1">Ground Floor</option>
                                                <option value="2">1<sup>st</sup> Floor</option>
                                                <option value="3">2<sup>nd</sup> Floor</option>
                                                <option value="4">3<sup>rd</sup> Floor</option>
                                                <option value="5">4<sup>th</sup> Floor</option>
                                                <option value="6">5<sup>th</sup> Floor</option>
                                                <option value="7">6<sup>th</sup> Floor</option>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-12 col-lg-12">
                                        <div class="modal-header"></div>
                                            <br>
                                            <input type="submit" class="btn btn-primary" value="Submit">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->

    </div>
    <!-- /#page-wrapper -->
</div>
<!-- /#wrapper -->
<?php $this->load->view('scriptlinks/scriptlinks.php'); ?>
<script>
$(document).ready(function () {

    //datatable initialization
     var table=$('#helpdesks').DataTable({responsive:true,"iDisplayLength": 10,"lengthMenu": [[10, 25, 50, 100, 200, -1], [10, 25, 50, 100, 200, "All"]]
   });
});
//to refresh the page
$( "#refresh").click( function(event)
    {
        window.setTimeout(function(){location.reload()},1)

    });
</script>
</body>
</html>
