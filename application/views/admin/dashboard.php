<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>SGS - Type of Visit</title>
   <?php $this->load->view('headerlinks/headerlinks.php'); ?>
   <link href="<?php echo base_url(); ?>assets/css/core.css" rel="stylesheet" type="text/css" />
   <link href="<?php echo base_url(); ?>assets/general-css/smsgeneral.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div id="wrapper">
    <?php $this->load->view('admin/officernav.php'); ?><!--navigation -->
    <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h4 class="page-header" style="margin-top:10px;color:darkgrey">Type of Visit</h4>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            </span>
                <?php $msg = $this->session->flashdata('msg');
                $successful= $msg['success']; $failed=  $msg['error']; if ($successful=="" && $failed!=""){ echo '
                <div class="messagebox alert alert-danger" style="display: block">
                        <button type="button" class="close" data-dismiss="alert">*</button>
                        <div class="cs-text">
                            <i class="fa fa-close"></i>
                            <strong><span>';echo $msg['error']; echo '</span></strong>
                        </div> 
                </div>';}else if($successful=="" && $failed==""){echo '<div></div>';} else if ($successful!="" && $failed==""){ echo '
                <div class="messagebox alert alert-success" style="display: block">
                        <button type="button" class="close" data-dismiss="alert">*</button>
                        <div class="cs-text">
                            <i class="fa fa-check-circle-o"></i>
                            <strong><span>';echo $msg['success'];echo '</span></strong>
                        </div> 
                </div>';}?>
                <!-- Icon Cards -->
      
        <div class="row">

          <div class="col-md-6 col-lg-6">
           <a href="#" class="card-footer text-white clearfix small z-1 text-center">
            <div class="card text-white bg-info o-hidden h-100">
                <h2 class="text-white"><strong><span class="float-left">Official Visit </span>
                <span class="float-right">
                  <i class="fa fa-angle-right"></i> Phase 1
                </span></strong></h2>
            </div>
            </a>
          </div>

          <div class="col-md-6 col-lg-6">
           <a href="#" class="card-footer text-white clearfix small z-1 text-center">
            <div class="card text-white bg-warning o-hidden h-100">
                <h2 class="text-white"><strong><span class="float-left">Private Visit</span>
                <span class="float-right">
                  <i class="fa fa-angle-right"></i> Phase 1
                </span></strong></h2>
            </div>
            </a>
          </div>
          <div class="col-md-6 col-lg-6">
           <a href="#" class="card-footer text-white clearfix small z-1 text-center">
            <div class="card text-white bg-danger o-hidden h-100">
                <h2 class="text-white"><strong><span class="float-left">Official Visit</span>
                <span class="float-right">
                  <i class="fa fa-angle-right"></i> Phase 2
                </span></strong></h2>
            </div>
            </a>
          </div>
          <div class="col-md-6 col-lg-6">
           <a href="#" class="card-footer text-white clearfix small z-1 text-center">
            <div class="card text-white bg-primary o-hidden h-100">
                <h2 class="text-white"><strong><span class="float-left">Private Visit </span>
                <span class="float-right">
                  <i class="fa fa-angle-right"></i> Phase 2
                </span></strong></h2>
            </div>
            </a>
          </div>

          

        </div>
            
    </div>
    <!-- /#page-wrapper -->
</div>
<!-- /#wrapper -->
<?php $this->load->view('scriptlinks/scriptlinks.php'); ?>
<script>
$(document).ready(function () {
    var  submitBtn = $('input[type="submit"]');
        // allWells.show();
    submitBtn.click(function(){
        var curStep = $(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            curInputs = curStep.find("input,select"),
            isValid = true;
        $(".form-group").removeClass("has-error");
        for(var i=0; i<curInputs.length; i++){
            if (!curInputs[i].validity.valid){
                isValid = false;
                $(curInputs[i]).closest(".form-group").addClass("has-error");
            }
        }
        if (isValid)
            nextStepWizard.removeAttr('disabled').trigger('click');
    });
});
//to refresh the page
$( "#refresh").click( function(event)
    {
        window.setTimeout(function(){location.reload()},1)

    });
</script>
</body>
</html>
