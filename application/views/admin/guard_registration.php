<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SGS - Security Guard Registration</title>
    
   <?php $this->load->view('headerlinks/headerlinks.php'); ?>
   <link href="<?php echo base_url(); ?>assets/css/core.css" rel="stylesheet" type="text/css" />
   <link href="<?php echo base_url(); ?>assets/general-css/smsgeneral.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div id="wrapper">
    <?php $this->load->view('admin/adminnav.php'); ?><!--navigation -->
    <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h4 class="page-header" style="margin-top:10px;color:darkgrey">Security Guard Registration</h4>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            
            <span data-placement="top" data-toggle="tooltip" title="Add Security Guard">
                    <button class="btn btn-primary btn-s" data-title="Add Security guard" data-toggle="modal" data-target="#guard-registration-modal" ><span class="fa fa-plus-circle"></span>&nbsp;Add Security Guard</button>
            </span>

             <span data-placement="top" data-toggle="tooltip" title="Refresh"><button class="btn btn-s" data-title="Refresh "  id="refresh" ><span class="fa fa-refresh"></span>&nbsp;Refresh</button>
            </span>
            <span data-placement="top" data-toggle="tooltip" title="Print All">
                    <a class="btn btn-s" data-title="Print All" type="button" href="<?php echo base_url('');?>"><span class="fa fa-print"></span>&nbsp;Print All</a>
            </span>
            <br><br>
                <?php $msg = $this->session->flashdata('msg');
                $successful= $msg['success']; $failed=  $msg['error']; if ($successful=="" && $failed!=""){ echo '
                <div class="messagebox alert alert-danger" style="display: block">
                        <button type="button" class="close" data-dismiss="alert">*</button>
                        <div class="cs-text">
                            <i class="fa fa-close"></i>
                            <strong><span>';echo $msg['error']; echo '</span></strong>
                        </div> 
                </div>';}else if($successful=="" && $failed==""){echo '<div></div>';} else if ($successful!="" && $failed==""){ echo '
                <div class="messagebox alert alert-success" style="display: block">
                        <button type="button" class="close" data-dismiss="alert">*</button>
                        <div class="cs-text">
                            <i class="fa fa-check-circle-o"></i>
                            <strong><span>';echo $msg['success'];echo '</span></strong>
                        </div> 
                </div>';}?>
            <div class="row">
                <div class="col-md-12">
                    <table  class="table table-striped table-bordered table-hover display responsive nowrap" cellspacing="0" width="100%" id="securityguards"  >
                        <thead>
                            <tr>
                                <th class="text-center">Full Name</th>
                                <th class="text-center">Staff ID</th>
                                <th class="text-center">Phase</th>
                                <th class="text-center"></th>
                             </tr>
                        </thead>
                        <tbody >
                            <?php  foreach($guards as $guard){ 
                               ?>
                            <tr>
                                <td class="text-left"><?php  echo $guard['guard_fname']. " ".$guard['guard_lname']; ?></td>
                                <td class="text-center"><?php  echo $guard['guard_staff_id'];  ?></td>
                                <td class="text-center"><?php  echo $guard['phase_name']; ?></td>
                                <td class="text-center">
                                     <form style="display:inline;" name=<?php echo '"formEdit_'. $guard['guard_staff_id'].'"';  ?> method="post" action="<?php echo base_url('MC/editGuard');?>">
                                            <div class="form-group col-md-12 col-lg-12" style="display:none">
                                                <label for="staffId" class="control-label">Record ID*</label>
                                                <input required="required" class="form-control" name="staffId" id="staffId" placeholder="101" value="<?php echo $guard['guard_staff_id']; ?>">
                                            </div>
                                            <input class="btn btn-primary btn-s" data-title="Edit" id=<?php echo '"edit_'. $guard['guard_staff_id'].'"';  ?> name=<?php echo '"edit_'. $guard['guard_staff_id'].'"';  ?>  type="submit" value="Edit">
                                    </form>  
                                    <button class="btn btn-warning btn-s" data-title="Disable Guard"  id=<?php echo '"disable_'. $guard['guard_staff_id'].'"';  ?> name=<?php echo '"disable_'. $guard['guard_staff_id'].'"';?> value=<?php echo '"'.$guard['guard_staff_id'].'"';  ?> onclick="disGuardId(this);">Disable
                                    </button>
                                    </button>
                                </td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                    <!-- /.table-responsive -->
                </div>
            </div>
            <div id="guard-registration-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="full-width-modalLabel" aria-hidden="true" style="display: none;">
                <div class="modal-dialog modal-full">
                    <div class="modal-content">
                        <div class="modal-header" style="border-bottom: none!important;margin-bottom: -20px">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                <h4 class="modal-title" id="full-width-modalLabel" ><strong style="color: darkred">Security Guard Registration</strong></h4>
                        </div>
                        <div class="modal-body">
                            <form role="form" id="guard_registration" method="post" action="<?php echo base_url(); ?>MC/newGuard">
                                <div class="row setup-content" >
                                    <div class="col-s-12">
                                        <div class="form-group col-md-6 col-lg-6" >
                                            <label for="phaseId" class="control-label">Phase*</label>
                                            <select type="text" name="phaseId" class="form-control" id="phaseId" required="required">
                                                <option value="2">Phase II</option>
                                                <option value="1">Phase I</option>
                                            </select>
                                        </div>
                                        <div class="col-md-12">
                                             <div class="form-group col-md-6 col-lg-6 ">
                                                <label for="firstName" class="control-label">First Name*</label>
                                                <input type="text" name="firstName" placeholder="First Name" class="form-control" id="firstName" required="required">
                                            </div>
                                            <div class="form-group col-md-6 col-lg-6" >
                                                <label for="lastName" class="control-label">Last Name*</label>
                                                <input type="text" name="lastName" placeholder="Last Name" class="form-control" id="lastName" required="required">
                                            </div>
                                            <div class="form-group col-md-6 col-lg-6">
                                                <label for="otherNames" class="control-label">Other Names</label>
                                                <input type="text" name="otherNames" placeholder="Other Names" class="form-control" id="otherNames" >
                                            </div>
                                            <div class="form-group col-md-6 col-lg-6">
                                                <label for="staffID" class="control-label">Staff ID*</label>
                                                <input type="text" name="staffID" placeholder="Staff ID" class=" form-control" id="staffID" required="required">
                                            </div>
                                            <div class="form-group col-md-6 col-lg-6">
                                                <label for="userName" class="control-label">Username*</label>
                                                <input type="text" name="userName" placeholder="e.g. smokoro" class=" form-control" id="userName" required="required">
                                            </div>
                                            <div class="form-group col-md-6 col-lg-6">
                                                <label for="nationalID" class="control-label">National ID No.*</label>
                                                <input type="text" name="nationalID" placeholder="National ID No." class=" form-control" id="nationalID" required="required">
                                            </div>
                                            <div class="form-group col-md-6 col-lg-6">
                                                <label for="phoneNumber" class="control-label"> Current Phone No.*</label>
                                                <input type="text" name="phoneNumber" placeholder="Current Phone Number" class=" form-control" id="phoneNumber" required="required">
                                            </div>
                                            <div class="form-group col-md-6 col-lg-6">
                                                <label for="emailAddress" class="control-label"> Current Email Address*</label>
                                                <input type="text" name="emailAddress" placeholder="Current Email Address" class=" form-control" id="emailAddress" required="required">
                                            </div>
                                            <div class="form-group col-md-12 col-lg-12">
                                            <div class="modal-header"></div>
                                                <br>
                                                <input type="submit" class="btn btn-primary" value="Submit">
                                                <input type="reset" class="btn btn-default" value="Reset">
                                                <input type="button" class="btn btn-danger pull-right" data-dismiss="modal" value="Close">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->

            <div class="modal fade" id="disable_guard" tabindex="-1" role="dialog" aria-labelledby="Disable" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-body ">
                            <button class="btn btn-danger" style="width: 100%!important;"><span class="pull-left"><span class="fa fa-warning"></span> Are you sure you want to disable this guard?</span></button>
                            <form role="form" method="post"  id="guard_disable" action="<?php echo base_url(); ?>MC/disableGuard">
                                <div class="form-group col-md-12 col-lg-12" hidden="true">
                                    <label for="staffId" class="control-label">guard Id</label>
                                    <input type="text" name="staffId" placeholder="Staff Id" class=" form-control" id="staffId" required="required" readonly="true" >
                                </div>
                                <br><br>
                                <div class="form-group col-md-12 col-lg-12 " >
                                    <label for="reason_disable" class="control-label">Reason to disable guard*</label>
                                    <textarea required="required" class="form-control" name="reasonToDisable" placeholder="e.g. Transfer" ></textarea>
                                </div>
                                <button type="submit" class="btn btn-warning" id="disable_confirm"><span class="fa fa-check"></span> PROCEED</button>
                                <button type="button" class="btn " data-dismiss="modal"><span class="fa fa-remove"></span> Exit</button>
                            </form>
                        </div>
                    </div>
                    <!-- /.modal-content --> 
                </div>
                <!-- /.modal-dialog --> 
            </div>
    </div>
    <!-- /#page-wrapper -->
</div>
<!-- /#wrapper -->
<?php $this->load->view('scriptlinks/scriptlinks.php'); ?>
<script>
$(document).ready(function () {
     //datatable initialization
     var table=$('#securityguards').DataTable({responsive:true,"iDisplayLength": 5,"lengthMenu": [[5, 25, 50, 100, 200, -1], [5, 25, 50, 100, 200, "All"]],columnDefs: [ { orderable: false, targets: [3] }]
   });
    var  submitBtn = $('input[type="submit"]');
        // allWells.show();
    submitBtn.click(function(){
        var curStep = $(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            curInputs = curStep.find("input,select"),
            isValid = true;
        $(".form-group").removeClass("has-error");
        for(var i=0; i<curInputs.length; i++){
            if (!curInputs[i].validity.valid){
                isValid = false;
                $(curInputs[i]).closest(".form-group").addClass("has-error");
            }
        }
        if (isValid)
            nextStepWizard.removeAttr('disabled').trigger('click');
    });
});
//to refresh the page
$( "#refresh").click( function(event)
    {
        window.setTimeout(function(){location.reload()},1)

    });
</script>
</body>
</html>
