     <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom:0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#"></a>
            </div>
            <!-- /.navbar-header -->
            <ul class="nav navbar-top-links navbar-right" >
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                     <?php echo $this->session->userdata('adminName');//session to show who is logged in?>
                        <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <!-- <li><a href="<?php //echo base_url('Home/viewuserprofile?userid='); echo $this->session->userdata('adminID'); ?>"><i class="fa fa-user fa-fw"></i> User Profile</a>
                        </li> -->
                        <li class="divider"></li>
                        <li><a href="<?php echo base_url('LoginCtrl/logoutadmin'); ?>"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->
            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                      
                        <!--manage users-->
                        <li>
                            <a href="#"><i class="fa fa-group fa-fw"></i> Manage Users<span class="fa arrow"></span></a>
                             <ul class="nav nav-second-level">
                                <li>
                                    <a href="<?php echo base_url();?>MC/adminsView"><i class="fa fa-caret-right fa-fw"></i>&nbsp; Admin</a>
                                </li>

                                 <li>
                                    <a href="<?php echo base_url();?>MC/guardsView"><i class="fa fa-caret-right fa-fw"></i>&nbsp; Security Guards</a>
                                </li>
                            </ul>
                            <!--second-level-->
                        </li>
                        <!--manage users-->
                        
                        <!--manage guests-->
                        <li>
                            <a href="#"><i class="fa fa-group fa-fw fa-2x"></i> Guests<span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level">
                                    <li>
                                        <a href="<?php echo base_url();?>MC/dailyGuests"><i class="fa fa-caret-right fa-fw"></i>&nbsp;<?php echo date("D M d, Y");?> Guests </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url();?>MC/allGuests"><i class="fa fa-caret-right fa-fw"></i>&nbsp;All Guests </a>
                                    </li>
                                </ul>
                                <!--second-level-->
                        </li>
                         <!--manage guests-->
                        <li>
                            <a href="#"><i class="fa fa-id-badge fa-fw fa-2x"></i> Access Permits<span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level">
                                    <li>
                                        <a href="#"><i class="fa fa-caret-right fa-fw"></i>&nbsp;<?php echo date("D M d, Y");?> Permits <span class="fa arrow"></span></a></a>
                                            <ul class="nav nav-third-level">
                                                <li>
                                                    <a href="<?php echo base_url();?>MC/leoActivePermits"><i class="fa fa-caret-right fa-fw"></i>&nbsp;Active Permits </a>
                                                </li>
                                                <li>
                                                    <a href="<?php echo base_url();?>MC/leoClosedPermits"><i class="fa fa-caret-right fa-fw"></i>&nbsp; Past Permits</a>
                                                </li>
                                            </ul>
                                        <!--third-level-->
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url();?>MC/allCurrentPermits"><i class="fa fa-caret-right fa-fw"></i>&nbsp; All Current Permits</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url();?>MC/allPastPermits"><i class="fa fa-caret-right fa-fw"></i>&nbsp; All Past Permits</a>
                                    </li>
                                </ul>
                                <!--second-level-->
                        </li>
                        
                        <!--Manage Conferences-->
                        <li>
                            <a href="#"><i class="fa fa-th-list fa-fw fa-2x"></i> Manage Conferences<span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level">
                                    <li>
                                        <a href="<?php echo base_url();?>MC/upcomingConf"><i class="fa fa-caret-right fa-fw"></i>&nbsp;Upcoming Conferences</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url();?>MC/ongoingConf"><i class="fa fa-caret-right fa-fw"></i>&nbsp;Ongoing Conferences</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url();?>MC/closedConf"><i class="fa fa-caret-right fa-fw"></i>&nbsp;Closed Conferences </a>
                                    </li>
                                </ul>
                                <!--second-level-->
                        </li>
                        <!--end manage guests-->
                         <li class="text-center">
                            <a href="#" style="background-color: maroon;font-weight: bold;color:white"><span><i class="fa fa-copyright fa-fw fa-2x"></i>2017 Phenom Research Lab</span></a>
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>
