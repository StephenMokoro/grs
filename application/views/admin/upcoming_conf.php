<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SGS - Upcoming Conferences</title>
    
   <?php $this->load->view('headerlinks/headerlinks.php'); ?>
   <link href="<?php echo base_url(); ?>assets/css/core.css" rel="stylesheet" type="text/css" />
   <link href="<?php echo base_url(); ?>assets/general-css/smsgeneral.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div id="wrapper">
    <?php $this->load->view('admin/officernav.php'); ?><!--navigation -->
    <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h4 class="page-header" style="margin-top:10px;color:darkgrey"> <?php echo date("D M d, Y");?> : Upcoming Conferences</h4>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            
            <span data-placement="top" data-toggle="tooltip" title="Add Conference">
                    <button class="btn btn-info btn-s" data-title="Add Conference" data-toggle="modal" data-target="#conf-registration-modal" ><span class="fa fa-plus-circle"></span>&nbsp;Add Conference</button></span>

             <span data-placement="top" data-toggle="tooltip" title="Refresh"><button class="btn btn-s" data-title="Refresh "  id="refresh" ><span class="fa fa-refresh"></span>&nbsp;Refresh</button>
            </span>
            <span data-placement="top" data-toggle="tooltip" title="Print All">
                    <a class="btn btn-s" data-title="Print All" type="button" href="<?php echo base_url('');?>"><span class="fa fa-print"></span>&nbsp;Print All</a>
            </span>
            <br><br>
                <?php $msg = $this->session->flashdata('msg');
                $successful= $msg['success']; $failed=  $msg['error']; $state= $msg['state']['confName'];
                if ($successful=="" && $state=="" && $failed!=""){ echo '
                <div class="messagebox alert alert-danger" style="display: block">
                        <button type="button" class="close" data-dismiss="alert">*</button>
                        <div class="cs-text">
                            <i class="fa fa-close"></i>
                            <strong><span>';echo $msg['error']; echo '</span></strong>
                        </div> 
                </div>';}else if($successful=="" && $failed=="" && $state==""){echo '<div></div>';} else if ($successful!="" && $failed=="" && $state=="" ){ echo '
                <div class="messagebox alert alert-success" style="display: block">
                        <button type="button" class="close" data-dismiss="alert">*</button>
                        <div class="cs-text">
                            <i class="fa fa-check-circle-o"></i>
                            <strong><span>';echo $msg['success'];echo '</span></strong>
                        </div> 
                </div>';}else if ($successful=="" && $failed=="" && $state!=""){ echo '
                <div class="messagebox alert alert-danger" style="display: block">
                        <button type="button" class="close" data-dismiss="alert">*</button>
                        <div class="cs-text">
                            <strong><span>';echo ' &nbsp;&nbsp;

                        <form style="display:inline;" name='; echo '"formMarkExit_'. $state.'"';  echo 'method="post" action="'; echo base_url("MC/guestExit");echo '">
                            <div class="form-group col-md-12 col-lg-12" style="display:none">
                                <label for="entryRecordId" class="control-label">Entry Record ID*</label>
                                <input required="required" class="form-control" name="entryRecordId" id="entryRecordId" placeholder="101" value="';  echo $msg['state']['recordId']; echo '">
                            </div>

                            <button class="btn btn-warning btn-s" data-title="Guest Exit" id='; echo '"exitGuest'. $state.'"';  echo ' name='; echo '"exitGuest'. $state.'"';  echo 'type="submit" ><span class="fa fa-exclamation-circle "><span style="font-weight:bolder"> Release '. $msg['state']['fullName'].'</span></span> 
                           </button>';echo '
                        </form>';echo '</span></strong>
                        </div> 
                </div>';}?>
            <div class="row">
                <div class="col-md-12">
                    <table  class="table table-striped table-bordered table-hover display responsive nowrap" cellspacing="0" width="100%" id="upcomingConferences"  >
                        <thead>
                            <tr>
                                <th class="text-center">Conference Name</th>
                                <th class="text-center">Primary Venue</th>
                                <th class="text-center">Start Date</th>
                                <th class="text-center">End Date</th>
                                <th class="text-center"></th>
                             </tr>
                        </thead>
                        <tbody >
                            <?php  $count=1;
                            foreach($conferences as $conf){ 
                               ?>
                            <tr>
                                <td class="text-left"><?php  echo $conf['conf_name'];?></td>
                                <td class="text-center"><?php  echo $conf['conf_venue'];  ?></td>
                                <td class="text-center"><?php  echo $conf['conf_date_from'];  ?></td>
                                <td class="text-center"><?php  echo $conf['conf_date_to']; ?></td>
                                <td class="text-center">
                                      <form style="display:inline;" name=<?php echo '"formEdit_'. $conf['conf_auto_id'].'"';  ?> method="post" action="<?php echo base_url('MC/editConf');?>">
                                            <div class="form-group col-md-12 col-lg-12" style="display:none">
                                                <label for="confId" class="control-label">Conference Auto ID*</label>
                                                <input required="required" class="form-control" name="confId" id="confId" placeholder="101" value="<?php echo $conf['conf_auto_id']; ?>">
                                            </div>
                                        <span data-placement="top" data-toggle="tooltip" title="Edit Conference">
                                            <button class="btn btn-primary btn-s" data-title="Edit Conference" id=<?php echo '"edit_'. $conf['conf_auto_id'].'"';  ?> name=<?php echo '"edit_'. $conf['conf_auto_id'].'"';  ?>  type="submit" ><span class="fa fa-edit"></span>&nbsp;Edit </button>
                                        </span>
                                    </form> 
                                    <form style="display:inline;" name=<?php echo '"formCancel_'. $conf['conf_auto_id'].'"';  ?> method="post" action="<?php echo base_url('MC/cancelConf');?>">
                                            <div class="form-group col-md-12 col-lg-12" style="display:none">
                                                <label for="confId" class="control-label">Conference Auto ID*</label>
                                                <input required="required" class="form-control" name="confId" id="confId" placeholder="101" value="<?php echo $conf['conf_auto_id']; ?>">
                                            </div>

                                        <span data-placement="top" data-toggle="tooltip" title="Cancel Conference">
                                            <button class="btn btn-danger btn-s" data-title="Cancel Conference" id=<?php echo '"cancel_'. $conf['conf_auto_id'].'"';  ?> name=<?php echo '"cancel_'. $conf['conf_auto_id'].'"';  ?>  type="submit" ><span class="fa fa-times"></span>&nbsp;Cancel </button>
                                        </span>
                                    </form> 
                                </td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                    <!-- /.table-responsive -->
                </div>
            </div>
            <div id="conf-registration-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="full-width-modalLabel" aria-hidden="true" style="display: none;">
                <div class="modal-dialog modal-full">
                    <div class="modal-content">
                        <div class="modal-header" style="border-bottom: none!important;margin-bottom: -20px">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                <h4 class="modal-title" id="full-width-modalLabel" ><strong style="color: darkred">Conference registration</strong></h4>
                        </div>
                        <div class="modal-body">
                            <form role="form" id="conf_registration" method="post" action="<?php echo base_url(); ?>MC/newConference">
                                <div class="row setup-content" >
                                    <div class="col-xs-12">
                                        <div class="col-md-12">
                                             <div class="form-group col-md-6 col-lg-6 ">
                                                <label for="confName" class="control-label">Conference Name*</label>
                                                <input type="text" name="confName" placeholder=" 2017 Research Symposium" class="form-control" id="confName" required="required">
                                            </div>
                                            <div class="form-group col-md-6 col-lg-6">
                                                <label for="confVenue" class="control-label">Venue</label>
                                                <input type="text" name="confVenue" placeholder=" Main Auditorium" class="form-control" id="confVenue"  required="required">
                                            </div>
                                            <div class="form-group col-md-6 col-lg-6">
                                                <label for="confStartDate" class="control-label">Start Date*</label>
                                                <input type="text" name="confStartDate" placeholder="" class=" form-control" id="confStartDate" required="required">
                                            </div>
                                            <div class="form-group col-md-6 col-lg-6">
                                                <label for="confEndDate" class="control-label">End Date*</label>
                                                <input type="text" name="confEndDate" placeholder="" class=" form-control" id="confEndDate" required="required">
                                            </div>
                                             <div class="form-group col-md-12 col-lg-12">
                                                <label for="confDescription" class="control-label">Conference Description</label>
                                                <textarea type="text" name="confDescription" placeholder="" class=" form-control" id="confDescription" ></textarea>
                                            </div>
                                            <div class="form-group col-md-12 col-lg-12">
                                            <div class="modal-header"></div>
                                                <br>
                                                <input type="submit" class="btn btn-primary" value="Submit">
                                                <input type="reset" class="btn btn-default" value="Reset">
                                                <input type="button" class="btn btn-danger pull-right" data-dismiss="modal" value="Close">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
    </div>
    <!-- /#page-wrapper -->
</div>
<!-- /#wrapper -->
<?php $this->load->view('scriptlinks/scriptlinks.php'); ?>
<script>
$(document).ready(function () {
    //datatable initialization
     var table=$('#upcomingConferences').DataTable({responsive:true,"iDisplayLength": 5,"lengthMenu": [[5, 25, 50, 100, 200, -1], [5, 25, 50, 100, 200, "All"]],columnDefs: [ { orderable: false, targets: [4] }], "aaSorting": []
   });
    var  submitBtn = $('input[type="submit"]');
        // allWells.show();
    submitBtn.click(function(){
        var curStep = $(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            curInputs = curStep.find("input,select"),
            isValid = true;
        $(".form-group").removeClass("has-error");
        for(var i=0; i<curInputs.length; i++){
            if (!curInputs[i].validity.valid){
                isValid = false;
                $(curInputs[i]).closest(".form-group").addClass("has-error");
            }
        }
        if (isValid)
            nextStepWizard.removeAttr('disabled').trigger('click');
    });

    //date pickers
     $('#confStartDate').datepicker({
                format: "yyyy-mm-dd",
                todayHighlight: true,
                autoclose: true,
                calendarWeeks: false
                });
     $('#confEndDate').datepicker({
                format: "yyyy-mm-dd",
                todayHighlight: true,
                autoclose: true,
                calendarWeeks: false
                });
});
//to refresh the page
$( "#refresh").click( function(event)
    {
        window.setTimeout(function(){location.reload()},1)

    });
</script>
</body>
</html>
