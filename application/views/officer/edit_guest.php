<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SGS - Guest Profile Update</title>
    
   <?php $this->load->view('headerlinks/headerlinks.php'); ?>
   <link href="<?php echo base_url(); ?>assets/css/core.css" rel="stylesheet" type="text/css" />
   <link href="<?php echo base_url(); ?>assets/general-css/smsgeneral.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div id="wrapper">
    <?php $this->load->view('officer/officernav.php'); ?><!--navigation -->
    <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h4 class="page-header" style="margin-top:10px;color:darkgrey"> <?php echo date("D M d, Y");?> Guests</h4>
                </div>
                <!-- /.col-lg-12 -->
            </div>
                <div class="modal-body">
                    <form role="form" id="edit_guest" method="post" action="<?php echo base_url(); ?>MC/updateGuest">
                        <?php foreach ($guest as $gst){?>
                        <div class="row setup-content" >
                            <div class="col-xs-12">
                                <div class="col-md-12">
                                    <div class="form-group col-md-6 col-lg-6 " hidden="true">
                                        <label for="guestId" class="control-label">Guest UID*</label>
                                        <input type="text" name="guestId" placeholder="Guest UID" class="form-control" id="guestId" required="required" value=<?php echo '"'.$gst['guest_auto_id'].'"';?>>
                                    </div>
                                    <div class="form-group col-md-6 col-lg-6 ">
                                        <label for="firstName" class="control-label">First Name*</label>
                                        <input type="text" name="firstName" placeholder="First Name" class="form-control" id="firstName" required="required" value=<?php echo '"'.$gst['guest_fname'].'"';?>>
                                    </div>
                                    <div class="form-group col-md-6 col-lg-6" >
                                        <label for="lastName" class="control-label">Last Name*</label>
                                        <input type="text" name="lastName" placeholder="Last Name" class="form-control" id="lastName" required="required" value=<?php echo '"'.$gst['guest_lname'].'"';?>>
                                    </div>
                                    <div class="form-group col-md-6 col-lg-6">
                                        <label for="otherNames" class="control-label">Other Names</label>
                                        <input type="text" name="otherNames" placeholder="Other Names" class="form-control" id="otherNames" value=<?php echo '"'.$gst['guest_other_names'].'"';?>>
                                    </div>
                                    <div class="form-group col-md-6 col-lg-6">
                                        <label for="IdNo" class="control-label">National ID/Passport/Military*</label>
                                        <input type="text" name="IdNo" placeholder="National ID/Passport/Military" class=" form-control" id="IdNo" required="required" value=<?php echo '"'.$gst['guest_id'].'"';?>>
                                    </div>
                                    <div class="form-group col-md-6 col-lg-6">
                                        <label for="idType" class="control-label">Type of ID Used</label>
                                        <select type="text" name="idType" placeholder="ID Type" class=" form-control" id="idType" required="required">
                                        <
                                            <option value=<?php echo '"'.$gst['id_no'].'"';?>><?php echo $gst['id_type'];?></option>
                                            <option value="1">National ID</option>
                                            <option value="2">Passport</option>
                                            <option value="3">Military</option>
                                            <option value="4">Other</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-6 col-lg-6">
                                        <label for="phoneNumber" class="control-label"> Current Phone No.*</label>
                                        <input type="text" name="phoneNumber" placeholder="Current Phone Number" class=" form-control" id="phoneNumber" required="required" value=<?php echo '"'.$gst['guest_phone'].'"';?>>
                                    </div>
                                   
                                    <div class="form-group col-md-12 col-lg-12">
                                    <div class="modal-header"></div>
                                        <br>
                                        <input type="submit" class="btn btn-warning" value="Update">
                                        <input type="reset" class="btn btn-default" value="Reset">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php }?>
                    </form>
                </div>
    </div>
    <!-- /#page-wrapper -->
</div>
<!-- /#wrapper -->
<?php $this->load->view('scriptlinks/scriptlinks.php'); ?>
<script>
$(document).ready(function () {
    var  submitBtn = $('input[type="submit"]');
        // allWells.show();
    submitBtn.click(function(){
        var curStep = $(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            curInputs = curStep.find("input,select"),
            isValid = true;
        $(".form-group").removeClass("has-error");
        for(var i=0; i<curInputs.length; i++){
            if (!curInputs[i].validity.valid){
                isValid = false;
                $(curInputs[i]).closest(".form-group").addClass("has-error");
            }
        }
        if (isValid)
            nextStepWizard.removeAttr('disabled').trigger('click');
    });
});
//to refresh the page
$( "#refresh").click( function(event)
    {
        window.setTimeout(function(){location.reload()},1)

    });
</script>
</body>
</html>
