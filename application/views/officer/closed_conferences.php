<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SGS - Closed Conferences</title>
    
   <?php $this->load->view('headerlinks/headerlinks.php'); ?>
   <link href="<?php echo base_url(); ?>assets/css/core.css" rel="stylesheet" type="text/css" />
   <link href="<?php echo base_url(); ?>assets/general-css/smsgeneral.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div id="wrapper">
    <?php $this->load->view('officer/officernav.php'); ?><!--navigation -->
    <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h4 class="page-header" style="margin-top:10px;color:darkgrey"> <?php echo date("D M d, Y");?> : Closed Conferences</h4>
                </div>
                <!-- /.col-lg-12 -->
            </div>
             <span data-placement="top" data-toggle="tooltip" title="Refresh"><button class="btn btn-s" data-title="Refresh "  id="refresh" ><span class="fa fa-refresh"></span>&nbsp;Refresh</button>
            </span>
            <span data-placement="top" data-toggle="tooltip" title="Print All">
                    <a class="btn btn-s" data-title="Print All" type="button" href="<?php echo base_url('');?>"><span class="fa fa-print"></span>&nbsp;Print All</a>
            </span>
            <div class="row">
                <div class="col-md-12">
                    <table  class="table table-striped table-bordered table-hover display responsive nowrap" cellspacing="0" width="100%" id="ongoingConferences"  >
                        <thead>
                            <tr>
                                <th class="text-center">Conference Name</th>
                                <th class="text-center">Primary Venue</th>
                                <th class="text-center">Start Date</th>
                                <th class="text-center">End Date</th>
                                <th class="text-center"></th>
                             </tr>
                        </thead>
                        <tbody >
                            <?php  $count=1;
                            foreach($conferences as $conf){ 
                               ?>
                            <tr>
                                <td class="text-left"><?php  echo $conf['conf_name'];?></td>
                                <td class="text-center"><?php  echo $conf['conf_venue'];  ?></td>
                                <td class="text-center"><?php  echo $conf['conf_date_from'];  ?></td>
                                <td class="text-center"><?php  echo $conf['conf_date_to']; ?></td>
                                <td class="text-center">
                                      
                                </td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                    <!-- /.table-responsive -->
                </div>
            </div>
    </div>
    <!-- /#page-wrapper -->
</div>
<!-- /#wrapper -->
<?php $this->load->view('scriptlinks/scriptlinks.php'); ?>
<script>
$(document).ready(function () {
    //datatable initialization
     var table=$('#ongoingConferences').DataTable({responsive:true,"iDisplayLength": 10,"lengthMenu": [[10, 25, 50, 100, 200, -1], [10, 25, 50, 100, 200, "All"]],columnDefs: [ { orderable: false, targets: [4] }], "aaSorting": []
   });
   
});
//to refresh the page
$( "#refresh").click( function(event)
    {
        window.setTimeout(function(){location.reload()},1)

    });
</script>
</body>
</html>
