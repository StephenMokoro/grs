<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SGS - Daily Active Entry Permits</title>
    
   <?php $this->load->view('headerlinks/headerlinks.php'); ?>
   <link href="<?php echo base_url(); ?>assets/css/core.css" rel="stylesheet" type="text/css" />
   <link href="<?php echo base_url(); ?>assets/general-css/smsgeneral.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div id="wrapper">
    <?php $this->load->view('officer/officernav.php'); ?><!--navigation -->
    <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">

                    <h4 class="page-header" style="margin-top:10px;color:darkgrey"> Active Entry Permits for <?php echo date("D M d, Y");?> </h4>

                </div>
                <!-- /.col-lg-12 -->
            </div>
             <span data-placement="top" data-toggle="tooltip" title="Refresh"><button class="btn btn-s" data-title="Refresh "  id="refresh" ><span class="fa fa-refresh"></span>&nbsp;Refresh</button>
            </span>
            <span data-placement="top" data-toggle="tooltip" title="Print All">
                    <a class="btn btn-s" data-title="Print All" type="button" href="<?php echo base_url('');?>"><span class="fa fa-print"></span>&nbsp;Print All</a>
            </span>
            <br><br>
                
                <?php $msg = $this->session->flashdata('msg');
                $successful= $msg['success']; $failed=  $msg['error']; $state= $msg['state']['fullName'];
                if ($successful=="" && $state=="" && $failed!=""){ echo '
                <div class="messagebox alert alert-danger" style="display: block">
                        <button type="button" class="close" data-dismiss="alert">*</button>
                        <div class="cs-text">
                            <i class="fa fa-close"></i>
                            <strong><span>';echo $msg['error']; echo '</span></strong>
                        </div> 
                </div>';}else if($successful=="" && $failed=="" && $state==""){echo '<div></div>';} else if ($successful!="" && $failed=="" && $state=="" ){ echo '
                <div class="messagebox alert alert-success" style="display: block">
                        <button type="button" class="close" data-dismiss="alert">*</button>
                        <div class="cs-text">
                            <i class="fa fa-check-circle-o"></i>
                            <strong><span>';echo $msg['success'];echo '</span></strong>
                        </div> 
                </div>';}else if ($successful=="" && $failed=="" && $state!=""){ echo '
                <div class="messagebox alert alert-danger" style="display: block">
                        <button type="button" class="close" data-dismiss="alert">*</button>
                        <div class="cs-text">
                            <strong><span>';echo ' &nbsp;&nbsp;

                        <form style="display:inline;" name='; echo '"formMarkExit_'. $state.'"';  echo 'method="post" action="'; echo base_url("MC/guestExit");echo '">
                            <div class="form-group col-md-12 col-lg-12" style="display:none">
                                <label for="entryRecordId" class="control-label">Entry Record ID*</label>
                                <input required="required" class="form-control" name="entryRecordId" id="entryRecordId" placeholder="101" value="';  echo $msg['state']['recordId']; echo '">
                            </div>

                            <button class="btn btn-warning btn-s" data-title="Guest Exit" id='; echo '"exitGuest'. $state.'"';  echo ' name='; echo '"exitGuest'. $state.'"';  echo 'type="submit" ><span class="fa fa-exclamation-circle "><span style="font-weight:bolder"> Release '. $msg['state']['fullName'].'</span></span> 
                           </button>';echo '
                        </form>';echo '</span></strong>
                        </div> 
                </div>';}?>
            <div class="row">
                <div class="col-md-12">
                    <table  class="table table-striped table-bordered table-hover display responsive nowrap" cellspacing="0" width="100%" id="permitentries"  >
                        <thead>
                            <tr>
                                <th class="text-center">Full Name</th>
                                <th class="text-center">Id No</th>
                                <th class="text-center">Phone</th>
                                <th class="text-center">Card No.</th>
                                <th class="text-center">Entry Time</th>
                                <th class="text-center">Expected Out</th>
                                <th class="text-center">Visiting</th>
                                <th class="text-center"></th>
                             </tr>
                        </thead>
                        <tbody >
                            <?php  $count=1;
                            foreach($guests as $guest){ 
                               ?>
                            <tr>
                                <td class="text-left"><?php  echo $guest['guest_fname']. " ".$guest['guest_lname']; ?></td>
                                <td class="text-center"><?php  echo $guest['guest_id'];  ?></td>
                                <td class="text-center"><?php  echo $guest['guest_phone']; ?></td>
                                <td class="text-center"><?php  echo $guest['entry_card_no'];  ?></td>
                                <td class="text-center"><?php  echo $guest['entry_time'];  ?></td>
                                <td class="text-center"><?php  echo $guest['entry_expected_time_out'];  ?></td>
                                <td class="text-center"><?php  echo $guest['entry_office_to_visit'];  ?></td>
                                <td class="text-center">

                                    <?php $status=$guest['id_upload']; if($status==FALSE)
                                        { echo' <form style="display:inline;" name='; echo '"formViewGuest_'. $guest['guest_auto_id'].'"';  echo 'method="post" action="'; echo base_url('MC/viewGuest');echo '">
                                        <div class="form-group col-md-12 col-lg-12" style="display:none">
                                            <label for="guestId" class="control-label">Guest PID*</label>
                                            <input required="required" class="form-control" name="guestId" id="guestId" placeholder="101" value="'; echo $guest['guest_auto_id'];echo '">
                                        </div>
                                        <span data-placement="top" data-toggle="tooltip" title="View Guest">
                                        <button class="btn btn-default btn-s" data-title="View Guest" id='; echo '"viewGuest_'. $guest['guest_auto_id'].'"';  echo ' name='; echo '"viewGuest_'. $guest['guest_auto_id'].'"'; echo 'type="submit" ><span class="fa fa-eye-slash" style="color:red"></span>&nbsp;View Guest</button>
                                        </span>
                                    </form> ';
                                        }else {
                                                echo' <form style="display:inline;" name='; echo '"formViewGuest_'. $guest['guest_auto_id'].'"';  echo 'method="post" action="'; echo base_url('MC/viewGuest');echo '">
                                        <div class="form-group col-md-12 col-lg-12" style="display:none">
                                            <label for="guestId" class="control-label">Guest PID*</label>
                                            <input required="required" class="form-control" name="guestId" id="guestId" placeholder="101" value="'; echo $guest['guest_auto_id'];echo '">
                                        </div>
                                        <span data-placement="top" data-toggle="tooltip" title="View Guest">
                                        <button class="btn btn-default btn-s" data-title="View Guest" id='; echo '"viewGuest_'. $guest['guest_auto_id'].'"';  echo ' name='; echo '"viewGuest_'. $guest['guest_auto_id'].'"'; echo 'type="submit" ><span class="fa fa-eye" style="color:darkblue"></span>&nbsp;View Guest</button>
                                        </span>
                                    </form> ';}
                                            ?>
                                    
                                    <?php $releaseState=$guest['entry_guest_release']; if($releaseState==1 ||$releaseState=="" )
                                        { echo' <form style="display:inline;" name='; echo '"formEP_'. $guest['guest_auto_id'].'"';  echo 'method="post" action="'; echo base_url('MC/addPermEntry');echo '">
                                        <div class="form-group col-md-12 col-lg-12" style="display:none">
                                            <label for="guestId" class="control-label">Guest PID*</label>
                                            <input required="required" class="form-control" name="guestId" id="guestId" placeholder="101" value="'; echo $guest['guest_auto_id'];echo '">
                                        </div>
                                        <span data-placement="top" data-toggle="tooltip" title="Add Entry Permission">
                                        <button class="btn btn-info btn-s" data-title="Add Entry Permission" id='; echo '"newEP_'. $guest['guest_auto_id'].'"';  echo ' name='; echo '"newEP_'. $guest['guest_auto_id'].'"'; echo 'type="submit" ><span class="fa fa-plus-circle"></span> Add Permit</button>
                                        </span>
                                    </form> ';
                                        }else if($releaseState==0){
                                                echo' <form style="display:inline;" name='; echo '"formPExist_'. $guest['guest_auto_id'].'"';  echo 'method="post" action="'; echo base_url('MC/permError');echo '">
                                        <div class="form-group col-md-12 col-lg-12" style="display:none">
                                            <label for="guestId" class="control-label">Guest PID*</label>
                                            <input required="required" class="form-control" name="guestId" id="guestId" placeholder="101" value="'; echo $guest['guest_auto_id'];echo '">
                                        </div>
                                        <div class="form-group col-md-12 col-lg-12" style="display:none">
                                            <label for="permitRecordId" class="control-label">Permit Record Id*</label>
                                            <input required="required" class="form-control" name="permitRecordId" id="permitRecordId" placeholder="101" value="'; echo $guest['entry_auto_id'];echo '">
                                        </div>
                                        <div class="form-group col-md-12 col-lg-12" style="display:none">
                                            <label for="permitTime" class="control-label">Permit Time*</label>
                                            <input required="required" class="form-control" name="permitTime" id="permitTime" placeholder="101" value="'; echo $guest['entry_time'];echo '">
                                        </div>
                                        <span data-placement="top" data-toggle="tooltip" title=" Permit Exit">
                                        <button class="btn btn-danger btn-s" data-title="Permit Exit" id='; echo '"permError'. $guest['guest_auto_id'].'"';  echo ' name='; echo '"permError'. $guest['guest_auto_id'].'"'; echo 'type="submit" ><span class="fa fa-hand-peace-o"></span> Permit Exit</button>
                                        </span>
                                    </form> ';}
                                            ?>
                                </td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                    <!-- /.table-responsive -->
                </div>
            </div>
            
    </div>
    <!-- /#page-wrapper -->
</div>
<!-- /#wrapper -->
<?php $this->load->view('scriptlinks/scriptlinks.php'); ?>
<script>
$(document).ready(function () {

      //datatable initialization
     var table=$('#permitentries').DataTable({responsive:true,"iDisplayLength": 10,"lengthMenu": [[10, 25, 50, 100, 200, -1], [10, 25, 50, 100, 200, "All"]],columnDefs: [ { orderable: false, targets: [7] }], "aaSorting": []
   });
});
//to refresh the page
$( "#refresh").click( function(event)
    {
        window.setTimeout(function(){location.reload()},1)

    });

</script>
</body>
</html>
