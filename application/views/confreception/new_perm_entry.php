<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SGS - Entry Permits</title>
    
   <?php $this->load->view('headerlinks/headerlinks.php'); ?>
   <link href="<?php echo base_url(); ?>assets/css/core.css" rel="stylesheet" type="text/css" />
   <link href="<?php echo base_url(); ?>assets/general-css/smsgeneral.css" rel="stylesheet" type="text/css" />
   <link href="<?php echo base_url(); ?>assets/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
    </head>
<body>
<div id="wrapper">
    <?php $this->load->view('officer/officernav.php'); ?><!--navigation -->
    <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">

                    <h4 class="page-header" style="margin-top:10px;color:darkgrey"> <?php echo date("D M d, Y");?> </h4>

                </div>
                <!-- /.col-lg-12 -->
            </div>
            <div class="form-group col-md-12 col-lg-12">
                <?php $msg = $this->session->flashdata('msg');
                $successful= $msg['success']; $failed=  $msg['error']; $state= $msg['state'];
                if ($successful=="" && $state=="" && $failed!=""){ echo '
                <div class="messagebox alert alert-danger" style="display: block">
                        <button type="button" class="close" data-dismiss="alert">*</button>
                        <div class="cs-text">
                            <i class="fa fa-close"></i>
                            <strong><span>';echo $msg['error']; echo '</span></strong>
                        </div> 
                </div>';}else if($successful=="" && $failed=="" && $state==""){echo '<div></div>';} else if ($successful!="" && $failed=="" && $state=="" ){ echo '
                <div class="messagebox alert alert-success" style="display: block">
                        <button type="button" class="close" data-dismiss="alert">*</button>
                        <div class="cs-text">
                            <i class="fa fa-check-circle-o"></i>
                            <strong><span>';echo $msg['success'];echo '</span></strong>
                        </div> 
                </div>';}else if ($successful=="" && $failed=="" && $state!==""){ echo '
                <div class="messagebox alert alert-info" style="display: block">
                        <button type="button" class="close" data-dismiss="alert">*</button>
                        <div class="cs-text">
                            <i class="fa fa-info-circle"></i>
                            <strong><span>';echo 'No Remark added. &nbsp;&nbsp;

                        <form style="display:inline;" name='; echo '"formAddRemark_'. $state.'"';  echo 'method="post" action="'; echo base_url("MC/crmks_view");echo '">
                            <div class="form-group col-md-12 col-lg-12" style="display:none">
                                <label for="recordId" class="control-label">Record ID*</label>
                                <input required="required" class="form-control" name="recordId" id="recordId" placeholder="101" value="'; echo $state; echo '">
                            </div>
                            <button class="btn btn-primary btn-s" data-title="Add Remark" id='; echo '"addRemark_'. $state.'"';  echo ' name='; echo '"addRemark_'. $state.'"';  echo 'type="submit" ><span class="fa fa-plus-circle "> Add Remark</span> 
                           </button>
                        </form>';echo '</span></strong>
                        </div> 
                </div>';}else if ($successful=="" && $failed=="" && $state!==""){ echo '
                <div class="messagebox alert alert-danger" style="display: block">
                        <button type="button" class="close" data-dismiss="alert">*</button>
                        <div class="cs-text">
                            <i class="fa fa-warning"></i>
                            <strong><span>';echo 'Record exists.&nbsp;&nbsp;

                        <form style="display:inline;" name='; echo '"formstateIM_'. $state.'"';  echo 'method="post" action="'; echo base_url("MC/eimr_view");echo '">
                            <div class="form-group col-md-12 col-lg-12" style="display:none">
                                <label for="recordId" class="control-label">Record ID*</label>
                                <input required="required" class="form-control" name="recordId" id="recordId" placeholder="101" value="'; echo $state; echo '">
                            </div>
                            <button class="btn btn-default btn-s" data-title="state IM Record" id='; echo '"stateimrecord_'. $state.'"';  echo ' name='; echo '"stateimrecord_'. $state.'"';  echo 'type="submit" ><span class="fa fa-state "> state instead</span> 
                           </button>
                        </form>';echo '</span></strong>
                        </div> 
                </div>';}?>
                <div class="modal-body">
                    <form role="form" id="new_permit_entry" method="post" action="<?php echo base_url(); ?>MC/newPermit">
                        <div class="row setup-content" >
                            <div class="col-xs-12">
                                <div class="col-md-12">
                                <?php  foreach($guests as $guest){?>
                                     <div class="form-group col-md-6 col-lg-6 " hidden="true">
                                        <label for="guestId" class="control-label">Guest Auto Id</label>
                                        <input type="text" name="guestId" placeholder="Guest Auto Id" class="form-control" id="guestId" required="required" value=<?php echo '"'.$guest['guest_auto_id'].'"';   ?>>
                                    </div>
                                     
                                    <div class="form-group col-md-6 col-lg-6 ">
                                        <label for="fullName" class="control-label">Guest Full Name</label>
                                        <input type="text" name="fullName" placeholder="Guest's Full Name" class="form-control" id="fullName" required="required" disabled="true" value=<?php echo '"'.$guest['guest_fname']." ".$guest['guest_lname']." ".$guest['guest_other_names'].'"';   ?>>
                                    </div>
                                <?php  }?>
                                    <div class="form-group col-md-6 col-lg-6">
                                        <label for="visitType" class="control-label">Type of Visit</label>
                                        <select type="text" name="visitType" placeholder="Type of Visit" class=" form-control" id="visitType" required="required">
                                        <
                                            <option value="">--Please Select One--</option>
                                            <option value="1">Official </option>
                                            <option value="2">Personal</option>
                                            <option value="3">Other</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-6 col-lg-6">
                                        <label for="guestType" class="control-label">Type of Guest</label>
                                        <select type="text" name="guestType" placeholder="Type of Guest" class=" form-control" id="guestType" required="required">
                                        <
                                            <option value="">--Please Select One--</option>
                                            <option value="1">Contractor </option>
                                            <option value="2">General Guest</option>
                                            <option value="3">Other</option>
                                        </select>
                                    </div>

                                    
                                    <div class="form-group col-md-6 col-lg-6">
                                        <label for="minorsNo" class="control-label">No. of Minors</label>
                                        <input type="text" name="minorsNo" placeholder="No. of Minors" class="form-control" id="minorsNo" value="0">
                                    </div>
                                    <div class="form-group col-md-6 col-lg-6">
                                        <label for="cardNo" class="control-label">Visitors' Card No.</label>
                                        <input type="text" name="cardNo" placeholder="Visitors' Card No" class=" form-control" id="cardNo" required="required">
                                    </div>
                                    <div class="form-group col-md-6 col-lg-6" >
                                        <label for="timeOut" class="control-label">Expected Time Out*</label>
                                        <div class='input-group date' id='expectedTimeOut'>
                                                <input type='text' class="form-control" id="timeOut" name="timeOut" 
                                                        required="required" autocomplete="off" />
                                                <span class="input-group-addon">
                                                    <span class="fa fa-clock-o"></span>
                                                </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                     <div class="form-group col-md-6 col-lg-6">
                                        <label for="personToVisit" class="control-label">Person to Visit</label>
                                        <select type="text" name="personToVisit" placeholder="Person to Visit" class="form-control" id="personToVisit"></select>
                                    </div>
                                    <div class="form-group col-md-6 col-lg-6">
                                        <label for="officeToVisit" class="control-label">Office to Visit</label>
                                        <select type="text" name="officeToVisit" placeholder="e.g. Cafeteria" class=" form-control" id="officeToVisit"></select>
                                    </div>

                                    <div class="form-group col-md-12 col-lg-12">
                                    <div class="modal-header"></div>
                                        <br>
                                        <input type="submit" class="btn btn-primary" value="Submit">
                                        <input type="reset" class="btn btn-default" value="Reset">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
    </div>
    <!-- /#page-wrapper -->
</div>
<!-- /#wrapper -->
<?php $this->load->view('scriptlinks/scriptlinks.php'); ?>
<script src="<?php echo base_url();?>assets/select2/js/select2.min.js"></script>


<script>
$(document).ready(function () {
    //datatable initialization
    
    var  submitBtn = $('input[type="submit"]');
        // allWells.show();
    submitBtn.click(function(){
        var curStep = $(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            curInputs = curStep.find("input,select"),
            isValid = true;
        $(".form-group").removeClass("has-error");
        for(var i=0; i<curInputs.length; i++){
            if (!curInputs[i].validity.valid){
                isValid = false;
                $(curInputs[i]).closest(".form-group").addClass("has-error");
            }
        }
        if (isValid)
            nextStepWizard.removeAttr('disabled').trigger('click');
    });
     //autocomplete for person to visit
      $('#personToVisit').select2({
        placeholder: '--- Select Stratizen ---',
        ajax: {
          url: "<?php echo base_url('MC/getStratizen'); ?>",
          dataType: 'json',
          delay: 250,
          processResults: function (data) {
            return {
              results: data
            };
          },
          cache: true
        }
      });
    //autocomplete for office to visit
    //autocomplete
      $('#officeToVisit').select2({
        placeholder: '--- Select Office ---',
        ajax: {
          url: "<?php echo base_url('MC/getOffice'); ?>",
          dataType: 'json',
          delay: 250,
          processResults: function (data) {
            return {
              results: data
            };
          },
          cache: true
        }
      });

});

 $(function () 
    {
        $('#timeOut ').datetimepicker({
            format: 'LT'
        });
                 
    });
//to refresh the page
$( "#refresh").click( function(event)
    {
        window.setTimeout(function(){location.reload()},1)

    });

</script>
</body>
</html>
