<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>SGS - Helpdesk Officer Update</title>
   <?php $this->load->view('headerlinks/headerlinks.php'); ?>
   <link href="<?php echo base_url(); ?>assets/css/core.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div id="wrapper">
<?php $this->load->view('officer/officernav.php'); ?><!--navigation -->
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h4 class="page-header" style="margin-top:10px;color:grey">Update Helpdesk Officer</h4>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <br><br>
        <div class="form-group col-md-12 col-lg-12">
            <?php $msg = $this->session->flashdata('msg');
            $successful= $msg['success']; $failed=  $msg['error']; if ($successful=="" && $failed!=""){ echo '
            <div class="messagebox alert alert-danger" style="display: block">
                    <button type="button" class="close" data-dismiss="alert">*</button>
                    <div class="cs-text">
                        <i class="fa fa-close"></i>
                        <strong><span>';echo $msg['error']; echo '</span></strong>
                    </div> 
            </div>';}else if($successful=="" && $failed==""){echo '<div></div>';} else if ($successful!="" && $failed==""){ echo '
            <div class="messagebox alert alert-success" style="display: block">
                    <button type="button" class="close" data-dismiss="alert">*</button>
                    <div class="cs-text">
                        <i class="fa fa-check-circle-o"></i>
                        <strong><span>';echo $msg['success'];echo '</span></strong>
                    </div> 
            </div>';}?>
            <div class="modal-body">
            <?php foreach($hd_officer_profile as $profile){?>
                <form role="form" id="officer_registration" method="post" action="      <?php echo base_url(); ?>MC/updateHdOfficer">
                    <div class="row setup-content" >
                        <div class="col-xs-12">
                            <div class="form-group col-md-12 col-lg-12" >
                                <label for="helpdeskId" class="control-label">Helpdesk*</label>
                                <select type="text" name="helpdeskId" class="form-control" id="helpdeskId" required="required">
                                    <option value=<?php echo '"'.$profile['hd_officer_hd_auto_id'].'"';   ?>><?php echo $profile['hd_name'];   ?></option>
                                   
                                   <?php foreach($helpdesks as $hdesk){ ?>
                                    <option value=<?php echo '"'.$hdesk['hd_auto_id'].'"';   ?>><?php echo $hdesk['hd_name'];   }?></option>
                                </select>
                            </div>
                            <div class="form-group col-md-6 col-lg-6">
                                <label for="staffID" class="control-label">Staff ID*</label>
                                <input type="text" name="staffID" placeholder="Staff ID" class=" form-control" id="staffID" required="required" value=<?php echo '"'.$profile['hd_officer_staff_id'].'"';   ?> readonly="true">
                            </div>
                            
                            <div class="form-group col-md-6 col-lg-6 ">
                                <label for="firstName" class="control-label">First Name*</label>
                                <input type="text" name="firstName" placeholder="First Name" class="form-control" id="firstName" required="required" value=<?php echo '"'.$profile['hd_officer_fname'].'"';   ?>>
                            </div>
                            <div class="form-group col-md-6 col-lg-6" >
                                <label for="lastName" class="control-label">Last Name*</label>
                                <input type="text" name="lastName" placeholder="Last Name" class="form-control" id="lastName" required="required" value=<?php echo '"'.$profile['hd_officer_lname'].'"';   ?>>
                            </div>
                            <div class="form-group col-md-6 col-lg-6">
                                <label for="otherNames" class="control-label">Other Names</label>
                                <input type="text" name="otherNames" placeholder="Other Names" class="form-control" id="otherNames" value=<?php echo '"'.$profile['hd_officer_other_names'].'"';   ?>>
                            </div>
                            <div class="form-group col-md-6 col-lg-6">
                                <label for="nationalID" class="control-label">National ID No</label>
                                <input type="text" name="nationalID" placeholder="National ID No." class=" form-control" id="nationalID" required="required" value=<?php echo '"'.$profile['hd_officer_nid'].'"';   ?> >
                            </div>
                            <div class="form-group col-md-6 col-lg-6">
                                <label for="userName" class="control-label">Username*</label>
                                <input type="text" name="userName" placeholder="e.g. smokoro" class=" form-control" id="userName" required="required" value=<?php echo '"'.$profile['hd_officer_username'].'"';   ?>>
                            </div>
                            <div class="form-group col-md-6 col-lg-6">
                                <label for="phoneNumber" class="control-label"> Current Phone No.*</label>
                                <input type="text" name="phoneNumber" placeholder="Current Phone Number" class=" form-control" id="phoneNumber" required="required" value=<?php echo '"'.$profile['hd_officer_phone'].'"';   ?>>
                            </div>
                            <div class="form-group col-md-6 col-lg-6">
                                <label for="emailAddress" class="control-label"> Current Email Address*</label>
                                <input type="text" name="emailAddress" placeholder="Current Email Address" class=" form-control" id="emailAddress" required="required" value=<?php echo '"'.$profile['hd_officer_email'].'"';  } ?>>
                            </div>
                            <div class="form-group col-md-12 col-lg-12">
                            <div class="modal-header"></div>
                                <br>
                                <input type="submit" class="btn btn-warning pull-right" value="Update">
                            </div>
                        </div>
                    </div>
                </form>
            </div><!-- modal -->
        </div>
        <!-- /#page-wrapper -->
</div>
<!-- /#wrapper -->
<?php $this->load->view('scriptlinks/scriptlinks.php'); ?>
<script>
$(document).ready(function () {
    var  submitBtn = $('input[type="submit"]');
        // allWells.show();
    submitBtn.click(function(){
        var curStep = $(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            curInputs = curStep.find("input,select"),
            isValid = true;
        $(".form-group").removeClass("has-error");
        for(var i=0; i<curInputs.length; i++){
            if (!curInputs[i].validity.valid){
                isValid = false;
                $(curInputs[i]).closest(".form-group").addClass("has-error");
            }
        }
        if (isValid)
            nextStepWizard.removeAttr('disabled').trigger('click');
    });
});
//to refresh the page
$( "#refresh").click( function(event)
    {
        window.setTimeout(function(){location.reload()},1)

    });
</script>
</body>
</html>
