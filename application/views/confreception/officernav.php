     <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom:0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#"></a>
            </div>
            <!-- /.navbar-header -->
            <ul class="nav navbar-top-links navbar-right" >
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                     <?php echo $this->session->userdata('officerName');//session to show who is logged in?>
                        <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <!-- <li><a href="<?php //echo base_url('Home/viewuserprofile?userid='); echo $this->session->userdata('adminID'); ?>"><i class="fa fa-user fa-fw"></i> User Profile</a>
                        </li> -->
                        <li class="divider"></li>
                        <li><a href="<?php echo base_url('LoginCtrl/logoutadmin'); ?>"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->
            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li class="nav-item active" data-toggle="tooltip" data-placement="right" title="Dashboard">
                            <a class="nav-link" href="<?php echo base_url();?>MC/dashboard"">
                              <i class="fa fa-fw fa-dashboard"></i>
                              <span class="nav-link-text">
                                Dashboard</span>
                            </a>
                        </li>
                        <!--manage helpdesks-->
                        <li>
                            <a href="#"><i class="fa fa-building-o  fa-fw"></i> Manage Helpdesks<span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level">
                                    <li>
                                        <a href="<?php echo base_url();?>MC/helpdesksView"><i class="fa fa-caret-right fa-fw"></i>&nbsp; Helpdesk Registration</a>
                                    </li>

                                     <li>
                                        <a href="<?php echo base_url();?>MC/hdOfficersView"><i class="fa fa-caret-right fa-fw"></i>&nbsp; Helpdesk Officer</a>
                                    </li>
                                </ul>
                                <!--second-level-->
                        </li>
                        <!--end manage helpdesks-->
                        
                        <!--manage guests-->
                        <li>
                            <a href="#"><i class="fa fa-user fa-fw"></i> Manage Guests<span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level">
                                    <li>
                                        <a href="<?php echo base_url();?>MC/newGuests"><i class="fa fa-caret-right fa-fw"></i>&nbsp;New Guests </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url();?>MC/guestsView"><i class="fa fa-caret-right fa-fw"></i>&nbsp;All Guests </a>
                                    </li>

                                     <li>
                                        <a href="<?php echo base_url();?>MC/permEntries"><i class="fa fa-caret-right fa-fw"></i>&nbsp; Permitted Entries</a>
                                    </li>
                                </ul>
                                <!--second-level-->
                        </li>
                        
                        <!--Manage Conferences-->
                        <li>
                            <a href="#"><i class="fa fa-group fa-fw"></i> Manage Conferences<span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level">
                                    <li>
                                        <a href="<?php echo base_url();?>MC/closedConfView"><i class="fa fa-caret-right fa-fw"></i>&nbsp;Closed Conferences </a>
                                    </li>
                                    
                                    <li>
                                        <a href="<?php echo base_url();?>MC/addConfView"><i class="fa fa-caret-right fa-fw"></i>&nbsp;Add Conference</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url();?>MC/activeConfView"><i class="fa fa-caret-right fa-fw"></i>&nbsp;Ongoing Conferences</a>
                                    </li>

                                </ul>
                                <!--second-level-->
                        </li>
                        <!--end manage guests-->
                         <li class="text-center">
                            <a href="#" style="background-color: maroon;font-weight: bold;color:white"><span><i class="fa fa-copyright fa-fw"></i>2017 Phenom Research Lab</span></a>
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>
