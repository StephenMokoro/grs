<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SGS -  New Staff</title>
    
   <?php $this->load->view('headerlinks/headerlinks.php'); ?>
   <link href="<?php echo base_url(); ?>assets/css/core.css" rel="stylesheet" type="text/css" />
   <link href="<?php echo base_url(); ?>assets/general-css/smsgeneral.css" rel="stylesheet" type="text/css" />
   <link href="<?php echo base_url(); ?>assets/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div id="wrapper">
    <?php $this->load->view('hdofficer/officernav.php'); ?><!--navigation -->
    <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h4 class="page-header" style="margin-top:10px;color:darkgrey">New Staff </h4>
                </div>
                <!-- /.col-lg-12 -->
            </div>
                <?php $msg = $this->session->flashdata('msg');
                $successful= $msg['success']; $failed=  $msg['error']; $state= $msg['state']['fullName'];
                if ($successful=="" && $state=="" && $failed!=""){ echo '
                <div class="messagebox alert alert-danger" style="display: block">
                        <button type="button" class="close" data-dismiss="alert">*</button>
                        <div class="cs-text">
                            <i class="fa fa-close"></i>
                            <strong><span>';echo $msg['error']; echo '</span></strong>
                        </div> 
                </div>';}else if($successful=="" && $failed=="" && $state==""){echo '<div></div>';} else if ($successful!="" && $failed=="" && $state=="" ){ echo '
                <div class="messagebox alert alert-success" style="display: block">
                        <button type="button" class="close" data-dismiss="alert">*</button>
                        <div class="cs-text">
                            <i class="fa fa-check-circle-o"></i>
                            <strong><span>';echo $msg['success'];echo '</span></strong>
                        </div> 
                </div>';}else if ($successful=="" && $failed=="" && $state!=""){ echo '
                <div class="messagebox alert alert-danger" style="display: block">
                        <button type="button" class="close" data-dismiss="alert">*</button>
                        <div class="cs-text">
                            <strong><span>';echo ' &nbsp;&nbsp;

                        <form style="display:inline;" name='; echo '"formMarkExit_'. $state.'"';  echo 'method="post" action="'; echo base_url("MC/guestExit");echo '">
                            <div class="form-group col-md-12 col-lg-12" style="display:none">
                                <label for="entryRecordId" class="control-label">Entry Record ID*</label>
                                <input required="required" class="form-control" name="entryRecordId" id="entryRecordId" placeholder="101" value="';  echo $msg['state']['recordId']; echo '">
                            </div>

                            <button class="btn btn-warning btn-s" data-title="Guest Exit" id='; echo '"exitGuest'. $state.'"';  echo ' name='; echo '"exitGuest'. $state.'"';  echo 'type="submit" ><span class="fa fa-exclamation-circle "><span style="font-weight:bolder"> Release '. $msg['state']['fullName'].'</span></span> 
                           </button>';echo '
                        </form>';echo '</span></strong>
                        </div> 
                </div>';}?>
                <div class="modal-body">
                    <form role="form" id="stratizen_officer_registration" method="post" action="<?php echo base_url(); ?>MC/newStaff">
                        <div class="row setup-content" >
                            <div class="col-xs-12">
                                <div class="col-md-12">
                                    <div class="form-group col-md-6 col-lg-6" readonly="true">
                                        <label for="stratzCategory" class="control-label">Stratizen Category*</label>
                                        <select type="text" name="stratzCategory" class="form-control" id="stratzCategory" required="required">
                                            <option value="2">Staff</option>
                                        </select>
                                    </div>
                                     <div class="form-group col-md-6 col-lg-6 ">
                                        <label for="firstName" class="control-label">First Name*</label>
                                        <input type="text" name="firstName" placeholder="First Name" class="form-control" id="firstName" required="required">
                                    </div>
                                    <div class="form-group col-md-6 col-lg-6" >
                                        <label for="lastName" class="control-label">Last Name*</label>
                                        <input type="text" name="lastName" placeholder="Last Name" class="form-control" id="lastName" required="required">
                                    </div>
                                    <div class="form-group col-md-6 col-lg-6">
                                        <label for="otherNames" class="control-label">Other Names</label>
                                        <input type="text" name="otherNames" placeholder="Other Names" class="form-control" id="otherNames" >
                                    </div>
                                    <div class="form-group col-md-6 col-lg-6">
                                        <label for="staffID" class="control-label">Staff ID*</label>
                                        <input type="text" name="staffID" placeholder="Staff ID" class=" form-control" id="staffID" required="required">
                                    </div>
                                    <div class="form-group col-md-6 col-lg-6">
                                        <label for="extension" class="control-label">Staff Extension</label>
                                        <input type="text" name="extension" placeholder="Staff Extension" class=" form-control" id="extension" >
                                    </div>
                                   
                                    <div class="form-group col-md-6 col-lg-6">
                                        <label for="phoneNumber" class="control-label"> Current Phone No.*</label>
                                        <input type="text" name="phoneNumber" placeholder="Current Phone Number" class=" form-control" id="phoneNumber" required="required">
                                    </div>
                                    <div class="form-group col-md-6 col-lg-6">
                                        <label for="emailAddress" class="control-label"> Current Email Address*</label>
                                        <input type="text" name="emailAddress" placeholder="Current Email Address" class=" form-control" id="emailAddress" required="required">
                                    </div>
                                </div>
                                
                                <div class="col-md-12">
                                     <div class="form-group col-md-6 col-lg-6">
                                        <label for="staffRoom" class="control-label">Office/Staffroom</label>
                                        <select type="text" name="staffRoom" placeholder="Staffroom/Office" class="form-control" id="staffRoom"></select>
                                    </div>
                                    <div class="form-group col-md-12 col-lg-12">
                                    <div class="modal-header"></div>
                                        <br>
                                        <input type="submit" class="btn btn-primary" value="Submit">
                                        <input type="reset" class="btn btn-default" value="Reset">
                                        <input type="button" class="btn btn-danger pull-right" data-dismiss="modal" value="Close">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
    </div>
    <!-- /#page-wrapper -->
</div>
<!-- /#wrapper -->
<?php $this->load->view('scriptlinks/scriptlinks.php'); ?>
<script src="<?php echo base_url();?>assets/select2/js/select2.min.js"></script>
<script>
$(document).ready(function () {
    var  submitBtn = $('input[type="submit"]');
        // allWells.show();
    submitBtn.click(function(){
        var curStep = $(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            curInputs = curStep.find("input,select"),
            isValid = true;
        $(".form-group").removeClass("has-error");
        for(var i=0; i<curInputs.length; i++){
            if (!curInputs[i].validity.valid){
                isValid = false;
                $(curInputs[i]).closest(".form-group").addClass("has-error");
            }
        }
        if (isValid)
            nextStepWizard.removeAttr('disabled').trigger('click');
    });

    //autocomplete for Office/ Staffroom
      $('#staffRoom').select2({
        tags: true,
        placeholder: '--- Add or Select Office/Staffroom ---',
        ajax: {
          url: "<?php echo base_url('MC/getStaffroom'); ?>",
          dataType: 'json',
          delay: 250,
          processResults: function (data) {
            return {
              results: data
            };
          },
          cache: true
        }
      });

});
//to refresh the page
$( "#refresh").click( function(event)
    {
        window.setTimeout(function(){location.reload()},1)

    });
</script>
</body>
</html>
