     <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom:0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#"></a>
            </div>
            <!-- /.navbar-header -->
            <ul class="nav navbar-top-links navbar-right" >
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                     <?php echo $this->session->userdata('hdOfficerName');//session to show who is logged in?>
                        <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <!-- <li><a href="<?php //echo base_url('Home/viewuserprofile?userid='); echo $this->session->userdata('adminID'); ?>"><i class="fa fa-user fa-fw"></i> User Profile</a>
                        </li> -->
                        <li class="divider"></li>
                        <li><a href="<?php echo base_url('LoginCtrl/logoutadmin'); ?>"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->
            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li class="nav-item active" data-toggle="tooltip" data-placement="right" title="Dashboard">
                            <a class="nav-link" href="<?php echo base_url();?>MC/staffList">
                              <i class="fa fa-fw fa-dashboard"></i>
                              <span class="nav-link-text">
                                Dashboard</span>
                            </a>
                        </li>
                        <!--manage helpdesks-->
                        <li>
                            <a href="#"><i class="fa fa-building-o  fa-fw"></i> Manage Staff<span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level">
                                    <li>
                                        <a href="<?php echo base_url();?>MC/addStaff"><i class="fa fa-caret-right fa-fw"></i>&nbsp; Staff Registration</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url();?>MC/staffList"><i class="fa fa-caret-right fa-fw"></i>&nbsp; All Staff</a>
                                    </li>


                                     <li>
                                        <a href="<?php echo base_url();?>MC/staffLeft"><i class="fa fa-caret-right fa-fw"></i>&nbsp; Exited Staff</a>
                                    </li>
                                </ul>
                                <!--second-level-->
                        </li>
                        <!--end manage helpdesks-->
                        
                        <!--end manage helpdesk officers-->
                         <li class="text-center">
                            <a href="#" style="background-color: maroon;font-weight: bold;color:white"><span><i class="fa fa-copyright fa-fw"></i>2017 Phenom Research Lab</span></a>
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>
