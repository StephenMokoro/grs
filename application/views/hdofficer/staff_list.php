<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SGS -  All Staff</title>
    
   <?php $this->load->view('headerlinks/headerlinks.php'); ?>
   <link href="<?php echo base_url(); ?>assets/css/core.css" rel="stylesheet" type="text/css" />
   <link href="<?php echo base_url(); ?>assets/general-css/smsgeneral.css" rel="stylesheet" type="text/css" />
   <link href="<?php echo base_url(); ?>assets/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div id="wrapper">
    <?php $this->load->view('hdofficer/officernav.php'); ?><!--navigation -->
    <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h4 class="page-header" style="margin-top:10px;color:darkgrey">All Staff </h4>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <span data-placement="top" data-toggle="tooltip" title="Add Staff">
                    <a href="<?php echo base_url();?>MC/addStaff" class="btn btn-info btn-s" data-title="Add Staff" ><span class="fa fa-plus-circle"></span>&nbsp;Add Staff</a>
            </span>
            <span data-placement="top" data-toggle="tooltip" title="Refresh"><button class="btn btn-s" data-title="Refresh "  id="refresh" ><span class="fa fa-refresh"></span>&nbsp;Refresh</button>
            </span>
            <span data-placement="top" data-toggle="tooltip" title="Print All">
                <a class="btn btn-s" data-title="Print All" type="button" href="<?php echo base_url('');?>"><span class="fa fa-print"></span>&nbsp;Print All</a>
            </span>
            <br><br>
                <?php $msg = $this->session->flashdata('msg');
                $successful= $msg['success']; $failed=  $msg['error']; $state= $msg['state']['fullName'];
                if ($successful=="" && $state=="" && $failed!=""){ echo '
                <div class="messagebox alert alert-danger" style="display: block">
                        <button type="button" class="close" data-dismiss="alert">*</button>
                        <div class="cs-text">
                            <i class="fa fa-close"></i>
                            <strong><span>';echo $msg['error']; echo '</span></strong>
                        </div> 
                </div>';}else if($successful=="" && $failed=="" && $state==""){echo '<div></div>';} else if ($successful!="" && $failed=="" && $state=="" ){ echo '
                <div class="messagebox alert alert-success" style="display: block">
                        <button type="button" class="close" data-dismiss="alert">*</button>
                        <div class="cs-text">
                            <i class="fa fa-check-circle-o"></i>
                            <strong><span>';echo $msg['success'];echo '</span></strong>
                        </div> 
                </div>';}else if ($successful=="" && $failed=="" && $state!=""){ echo '
                <div class="messagebox alert alert-danger" style="display: block">
                        <button type="button" class="close" data-dismiss="alert">*</button>
                        <div class="cs-text">
                            <strong><span>';echo ' &nbsp;&nbsp;

                        <form style="display:inline;" name='; echo '"formMarkExit_'. $state.'"';  echo 'method="post" action="'; echo base_url("MC/guestExit");echo '">
                            <div class="form-group col-md-12 col-lg-12" style="display:none">
                                <label for="entryRecordId" class="control-label">Entry Record ID*</label>
                                <input required="required" class="form-control" name="entryRecordId" id="entryRecordId" placeholder="101" value="';  echo $msg['state']['recordId']; echo '">
                            </div>

                            <button class="btn btn-warning btn-s" data-title="Guest Exit" id='; echo '"exitGuest'. $state.'"';  echo ' name='; echo '"exitGuest'. $state.'"';  echo 'type="submit" ><span class="fa fa-exclamation-circle "><span style="font-weight:bolder"> Release '. $msg['state']['fullName'].'</span></span> 
                           </button>';echo '
                        </form>';echo '</span></strong>
                        </div> 
                </div>';}?>
                <div class="row">
                    <div class="col-md-12">
                        <table  class="table table-striped table-bordered table-hover display responsive nowrap" cellspacing="0" width="100%" id="allstaff"  >
                            <thead>
                                <tr>
                                    <th class="text-center">Full Name</th>
                                    <th class="text-center">Staff ID</th>
                                    <th class="text-center">Phone</th>
                                    <th class="text-center">Staffroom/Office</th>
                                    <th class="text-center"></th>
                                 </tr>
                            </thead>
                            <tbody >
                                <?php  foreach($staffs as $stratizen){ 
                                   ?>
                                <tr>
                                    <td class="text-left"><?php  echo $stratizen['stratizen_fname']. " ".$stratizen['stratizen_lname']; ?></td>
                                    <td class="text-center"><?php  echo $stratizen['stratizen_su_id'];  ?></td>
                                    <td class="text-center"><?php  echo $stratizen['stratizen_phone']; ?></td>
                                    <td class="text-center"><?php  echo $stratizen['stratizen_staffroom'];  ?></td>
                                    <td class="text-center">
                                        <form style="display:inline;" name=<?php echo '"formEdit_'. $stratizen['stratizen_auto_id'].'"';  ?> method="post" action="<?php echo base_url('MC/editStaff');?>">
                                                <div class="form-group col-md-12 col-lg-12" style="display:none">
                                                    <label for="staffId" class="control-label">Staff Auto ID*</label>
                                                    <input required="required" class="form-control" name="guestId" id="guestId" placeholder="101" value="<?php echo $stratizen['stratizen_auto_id']; ?>">
                                                </div>
                                            <span data-placement="top" data-toggle="tooltip" title="Edit Staff">
                                                <button class="btn btn-default btn-s" data-title="Edit Staff" id=<?php echo '"edit_'. $stratizen['stratizen_auto_id'].'"';  ?> name=<?php echo '"edit_'. $stratizen['stratizen_auto_id'].'"';  ?>  type="submit" ><span class="fa fa-edit"></span>&nbsp;Edit Staff</button>
                                            </span>
                                        </form> 
                                    </td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                        <!-- /.table-responsive -->
                    </div>
                </div>
                <div id="staff_officer-registration-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="full-width-modalLabel" aria-hidden="true" style="display: none;">
                <div class="modal-dialog modal-full">
                    <div class="modal-content">
                        <div class="modal-header" style="border-bottom: none!important;margin-bottom: -20px">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                <h4 class="modal-title" id="full-width-modalLabel" ><strong style="color: darkred">New Staff</strong></h4>
                        </div>
                        <div class="modal-body">
                            <form role="form" id="stratizen_officer_registration" method="post" action="<?php echo base_url(); ?>MC/newStratizen">
                                <div class="row setup-content" >
                                    <div class="col-xs-12">
                                        <div class="form-group col-md-6 col-lg-6" readonly="true">
                                            <label for="stratzCategory" class="control-label">Stratizen Category*</label>
                                            <select type="text" name="stratzCategory" class="form-control" id="stratzCategory" required="required">
                                                <option value="2">Staff</option>
                                            </select>
                                        </div>
                                        <div class="col-md-12">
                                             <div class="form-group col-md-6 col-lg-6 ">
                                                <label for="firstName" class="control-label">First Name*</label>
                                                <input type="text" name="firstName" placeholder="First Name" class="form-control" id="firstName" required="required">
                                            </div>
                                            <div class="form-group col-md-6 col-lg-6" >
                                                <label for="lastName" class="control-label">Last Name*</label>
                                                <input type="text" name="lastName" placeholder="Last Name" class="form-control" id="lastName" required="required">
                                            </div>
                                            <div class="form-group col-md-6 col-lg-6">
                                                <label for="otherNames" class="control-label">Other Names</label>
                                                <input type="text" name="otherNames" placeholder="Other Names" class="form-control" id="otherNames" >
                                            </div>
                                            <div class="form-group col-md-6 col-lg-6">
                                                <label for="staffID" class="control-label">Staff ID*</label>
                                                <input type="text" name="staffID" placeholder="Staff ID" class=" form-control" id="staffID" required="required">
                                            </div>
                                           
                                            <div class="form-group col-md-6 col-lg-6">
                                                <label for="phoneNumber" class="control-label"> Current Phone No.*</label>
                                                <input type="text" name="phoneNumber" placeholder="Current Phone Number" class=" form-control" id="phoneNumber" required="required">
                                            </div>
                                            <div class="form-group col-md-6 col-lg-6">
                                                <label for="emailAddress" class="control-label"> Current Email Address*</label>
                                                <input type="text" name="emailAddress" placeholder="Current Email Address" class=" form-control" id="emailAddress" required="required">
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-12">
                                             <div class="form-group col-md-6 col-lg-6">
                                                <label for="staffRoom" class="control-label">Office/Staffroom</label>
                                                <select type="text" name="staffRoom" placeholder="Staffroom/Office" class="form-control" id="staffRoom"></select>
                                            </div>
                                            <div class="form-group col-md-12 col-lg-12">
                                            <div class="modal-header"></div>
                                                <br>
                                                <input type="submit" class="btn btn-primary" value="Submit">
                                                <input type="reset" class="btn btn-default" value="Reset">
                                                <input type="button" class="btn btn-danger pull-right" data-dismiss="modal" value="Close">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->

    </div>
    <!-- /#page-wrapper -->
</div>
<!-- /#wrapper -->
<?php $this->load->view('scriptlinks/scriptlinks.php'); ?>
<script src="<?php echo base_url();?>assets/select2/js/select2.min.js"></script>
<script>
$(document).ready(function () {
    //datatable initialization
     var table=$('#allstaff').DataTable({responsive:true,"iDisplayLength": 5,"lengthMenu": [[5, 25, 50, 100, 200, -1], [5, 25, 50, 100, 200, "All"]],columnDefs: [ { orderable: false, targets: [4] }]
   });
    var  submitBtn = $('input[type="submit"]');
        // allWells.show();
    submitBtn.click(function(){
        var curStep = $(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            curInputs = curStep.find("input,select"),
            isValid = true;
        $(".form-group").removeClass("has-error");
        for(var i=0; i<curInputs.length; i++){
            if (!curInputs[i].validity.valid){
                isValid = false;
                $(curInputs[i]).closest(".form-group").addClass("has-error");
            }
        }
        if (isValid)
            nextStepWizard.removeAttr('disabled').trigger('click');
    });

    //autocomplete for Office/ Staffroom
      $('#staffRoom').select2({
        placeholder: '--- Select Office/Staffroom ---',
        ajax: {
          url: "<?php echo base_url('MC/getStaffroom'); ?>",
          dataType: 'json',
          delay: 250,
          processResults: function (data) {
            return {
              results: data
            };
          },
          cache: true
        }
      });


});
//to refresh the page
$( "#refresh").click( function(event)
    {
        window.setTimeout(function(){location.reload()},1)

    });
</script>
</body>
</html>
