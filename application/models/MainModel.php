<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
*Main Model to handle db connections
* @author Mokoro Stephen
*/
class MainModel extends CI_Model
{
function __construct()
{
     parent::__construct();
     $this->load->database();
}
//admin registration
public function newAdmin($admin_details)
{
    if($this->db->insert('admin',$admin_details))
        {
            return true;
        }
         else
        {
            return false;
        }
}

//admin update
public function updateAdmin($admin_details,$admin_staff_id)
{
    $this->db->where('admin_staff_id',$admin_staff_id);
    $this->db->update('admin',$admin_details);
    $affected=$this->db->affected_rows();
     if($affected>0)
            {
                return true;
            }else
                {
                    return false;
                }
}

//admin profile
public function adminProfile($staffId)
{
    $this->db->select('*');
    $this->db->from('admin');
    $this->db->where('admin_staff_id',$staffId);
    $result=$this->db->get()->result_array();
    return $result;
}
//list of all active admins
public function activeAdminsList()
{
    $this->db->select('*');
    $this->db->from('admin');
    $this->db->where('active_status',1);
    $result=$this->db->get()->result_array();
    return $result;
}

//disable admin
public function disableAdmin($updateDetails,$staffId)
{
    $this->db->where('admin_staff_id',$staffId);
    $this->db->update('admin',$updateDetails);
    $affected=$this->db->affected_rows();
     if($affected>0)
            {
                return true;

            }else
                {
                    return false;
                }
}

//Security Guard Registration
public function newGuard($guard_details)
{
	if($this->db->insert('guards',$guard_details))
        {
            return true;
        }
         else
        {
            return false;
        }
}

//Security Guard Profile update
public function updateGuard($guard_details,$staffId)
{
    $this->db->where('guard_staff_id',$staffId);
    $this->db->update('guards',$guard_details);
    $affected=$this->db->affected_rows();
     if($affected>0)
            {
                return true;
            }else
                {
                    return false;
                }
}

//Security Guard profile
public function guardProfile($staffId)
{
    $this->db->select('sg.*, ps.phase_name');
    $this->db->from('guards sg, phases ps');
    $this->db->where('sg.guard_active_status',1);
    $this->db->where('sg.guard_phase_id=ps.phase_id');
    $this->db->where('sg.guard_staff_id',$staffId);
    $result=$this->db->get()->result_array();
    return $result;
}
//list of all active security guards
public function activeGuardsList()
{
    $this->db->select('sg.*, ps.phase_name');
    $this->db->from('guards sg, phases ps');
    $this->db->where('sg.guard_active_status',1);
    $this->db->where('sg.guard_phase_id=ps.phase_id');
    $result=$this->db->get()->result_array();
    return $result;
}

//disable security guard
public function disableGuard($updateDetails,$staffId)
{
    $this->db->where('guard_staff_id',$staffId);
    $this->db->update('guards',$updateDetails);
    $affected=$this->db->affected_rows();
     if($affected>0)
            {
                return true;

            }else
                {
                    return false;
                }
}

//list of all guests
public function guestsList()
{
    $this->db->select('gs.*,id.id_type, ep.*');
    $this->db->from('guests gs, id_types id');
    $this->db->join('entry_permits ep', 'ep.entry_guest_auto_id = gs.guest_auto_id and ep.entry_guest_release=0', 'left');
    $this->db->where('gs.guest_id_type=id.id_no');
    $this->db->group_by('gs.guest_id');
    $this->db->order_by('gs.guest_auto_id','desc');
    $result=$this->db->get()->result_array();
    return $result;
}
//list of all guests for today
public function newGuestsList()
{
    $this->db->select('gs.*,id.id_type, ep.*');
    $this->db->from('guests gs, id_types id');
    $this->db->join('entry_permits ep', 'ep.entry_guest_auto_id = gs.guest_auto_id and ep.entry_guest_release=0', 'left');
    $this->db->where('gs.guest_id_type=id.id_no');
    $this->db->where('gs.date_registered',date("Y-m-d"));
    $this->db->group_by('gs.guest_id');
    $this->db->order_by('gs.guest_auto_id','desc');
    $result=$this->db->get()->result_array();
    return $result;
}
//list of all guests for today registered as Quick guests
public function quickGuestsList()
{
    $this->db->select('gs.*,id.id_type, ep.*');
    $this->db->from('guests gs, id_types id');
    $this->db->join('entry_permits ep', 'ep.entry_guest_auto_id = gs.guest_auto_id and ep.entry_guest_release=0', 'left');
    $this->db->where('gs.guest_fname=""');
    $this->db->where('gs.guest_lname=""');
    $this->db->where('gs.guest_id_type=id.id_no');
    $this->db->where('gs.date_registered',date("Y-m-d"));
    $this->db->group_by('gs.guest_id');
    $this->db->order_by('gs.guest_auto_id','desc');
    $result=$this->db->get()->result_array();
    return $result;
}
//update guest
public function updateGuest($guest_details,$guestId)
{
    $this->db->where('guest_auto_id',$guestId);
    $this->db->update('guests',$guest_details);
    $affected=$this->db->affected_rows();
     if($affected>0)
            {
                return true;
            }else
                {
                    return false;
                }
}

//buildings list
public function buildingsList()
{
    $this->db->select('*');
    $this->db->from('buildings');
    $result=$this->db->get()->result_array();
    return $result;
}
//helpdesk registration
public function newHelpdesk($helpdesk_details)
{
    if($this->db->insert('helpdesks',$helpdesk_details))
        {
            return true;
        }
         else
            {
                return false;
            }
}
//list of floors
public function floorsList()
{
    $this->db->select('*');
    $this->db->from('floors');
     $result=$this->db->get()->result_array();
    return $result;
}
//list of all helpdesks
public function helpdesksList()
{
    $this->db->select('hd.*, ph.phase_name,fl.floor_name,bd.building_name');
    $this->db->from('helpdesks hd, phases ph, floors fl,buildings bd');
    $this->db->where('ph.phase_id=hd.hd_phase_id');
    $this->db->where('hd.hd_floor_no=fl.floor_no');
    $this->db->where('hd.hd_building_id=bd.building_id');
    $this->db->order_by('hd.hd_name','asc');
    $result=$this->db->get()->result_array();
    return $result;
}
//get specific helpdesk for editing
public function getHelpdesk($hd_auto_id)
{
    $this->db->select('hd.*, ph.phase_name,fl.floor_name,bs.building_name');
    $this->db->from('helpdesks hd, phases ph, floors fl,buildings bs');
    $this->db->where('ph.phase_id=hd.hd_phase_id');
    $this->db->where('hd.hd_floor_no=fl.floor_no');
    $this->db->where('hd.hd_auto_id',$hd_auto_id);
    $this->db->where('hd.hd_building_id=bs.building_id');
    $result=$this->db->get()->result_array();
    return $result;
}
//function to update helpdesks
public function updateHelpdesk($helpdesk_details,$helpdeskId)
{
    $this->db->where('hd_auto_id',$helpdeskId);
    $this->db->update('helpdesks',$helpdesk_details);
    $affected=$this->db->affected_rows();
     if($affected>0)
            {
                return true;
            }else
                {
                    return false;
                }

}
//list of all active officers
public function activeHdStaffList()
{
    $this->db->select('hos.*, hds.hd_name');
    $this->db->from('hd_staff hos, helpdesks hds');
    $this->db->where('hos.hd_active_status',1);
    $this->db->where('hos.hd_staff_hd_auto_id=hds.hd_auto_id');
    $this->db->order_by('hos.hd_staff_fname','asc');
    $result=$this->db->get()->result_array();
    return $result;
}

//officer Helpdesk registration
public function newHdStaff($hd_staff_details)
{
    if($this->db->insert('hd_staff',$hd_staff_details))
        {
            return true;
        }
         else
        {
            return false;
        }
}

//officer Helpdesk update
public function updateHdStaff($hd_staff_details,$staffId)
{
    $this->db->where('hd_staff_id',$staffId);
    $this->db->update('hd_staff',$hd_Staff_details);
    $affected=$this->db->affected_rows();
     if($affected>0)
            {
                return true;
            }else
                {
                    return false;
                }
}
//Helpdesk officer profile
public function hdOfficerProfile($staffId)
{
    $this->db->select('hos.*, hds.hd_name');
    $this->db->from('hd_officers hos, helpdesks hds');
    $this->db->where('hos.hd_active_status',1);
    $this->db->where('hos.hd_officer_hd_auto_id=hds.hd_auto_id');
    $this->db->where('hos.hd_officer_staff_id',$staffId);
    $result=$this->db->get()->result_array();
    return $result;
}
//disable Helpdesk officer
public function disableHdOfficer($updateDetails,$staffId)
{
    $this->db->where('hd_officer_staff_id',$staffId);
    $this->db->update('hd_officers',$updateDetails);
    $affected=$this->db->affected_rows();
     if($affected>0)
            {
                return true;

            }else
                {
                    return false;
                }
}
public function conferencesList()
{
    $this->db->select('*');
    $this->db->from('conferences');
    $this->db->where('conf_status', TRUE);
    $result=$this->db->get()->result_array();
    return $result;
}
public function getGuest($guestId)
{
    $this->db->select('gs.*,id.*');
    $this->db->from('guests gs, id_types id');
    $this->db->where('gs.guest_id_type=id.id_no');
    $this->db->where('gs.guest_auto_id',$guestId);
    $result=$this->db->get()->result_array();
    return $result;
}
//New Guest  Registration
public function newGuest($guest_details)
{
    if($this->db->insert('guests',$guest_details))
        {
            return true;
        }
         else
        {
            return false;
        }
}

//daily active permits
public function todayClosedPermits()
{
    $this->db->select('ep.*,gs.*,is.id_type');
    $this->db->from('entry_permits ep,guests gs, id_types is');
    $this->db->where('ep.entry_guest_release',TRUE);
    $this->db->where('gs.guest_auto_id=ep.entry_guest_auto_id');
    $this->db->where('gs.guest_id_type=is.id_no');
    $this->db->where('ep.entry_date',date("Y-m-d"));
    $this->db->order_by('ep.entry_actual_time_out','desc');
     $result=$this->db->get()->result_array();
    return $result;
}
//daily closed permits
public function todayActivePermits()
{
    $this->db->select('ep.*,gs.*,is.id_type');
    $this->db->from('entry_permits ep,guests gs, id_types is');
    $this->db->where('ep.entry_guest_release',FALSE);
    $this->db->where('gs.guest_auto_id=ep.entry_guest_auto_id');
    $this->db->where('gs.guest_id_type=is.id_no');
    $this->db->where('ep.entry_date',date("Y-m-d"));
    $this->db->order_by('ep.entry_date');
     $result=$this->db->get()->result_array();
    return $result;
}
//all active permits
public function allActivePermits()
{
    $this->db->select('ep.*,gs.*,is.id_type');
    $this->db->from('entry_permits ep,guests gs, id_types is');
    $this->db->where('ep.entry_guest_release',FALSE);
    $this->db->where('gs.guest_auto_id=ep.entry_guest_auto_id');
    $this->db->where('gs.guest_id_type=is.id_no');
    $this->db->order_by('ep.entry_date');
     $result=$this->db->get()->result_array();
    return $result;
}
//all closed permits
public function allClosedPermits()
{
     $this->db->select('ep.*,gs.*,is.id_type');
    $this->db->from('entry_permits ep,guests gs, id_types is');
    $this->db->where('ep.entry_guest_release',TRUE);
    $this->db->where('gs.guest_auto_id=ep.entry_guest_auto_id');
    $this->db->where('gs.guest_id_type=is.id_no');
    $this->db->order_by('ep.entry_actual_time_out','desc');
    $result=$this->db->get()->result_array();
    return $result;
}
public function newPermit($permit_details)
{
     if($this->db->insert('entry_permits',$permit_details))
        {
            return true;
        }
         else
        {
            return false;
        }
}
//Release guest for exit
public function guestRelease($exitDetails,$entryRecordId)
{
    $this->db->where('entry_auto_id',$entryRecordId);
    $this->db->update('entry_permits',$exitDetails);
    $affected=$this->db->affected_rows();
     if($affected>0)
            {
                return true;
            }else
                {
                    return false;
                }
}
//all stratizens registered
public function stratizens()
{
    $this->db->select('str.*,ph.*,strtype.*');
    $this->db->from('stratizens str,phases ph,stratizen_types strtype');
    $this->db->where('str.stratizen_type_id=strtype.stratizen_type_id');
    $this->db->order_by('str.stratizen_fname');
    $this->db->order_by('str.stratizen_lname');
    $result=$this->db->get()->result_array();
    return $result;
}
//staff registration
public function allStaff()
{
    $this->db->select('str.*,ph.*,strtype.*');
    $this->db->from('stratizens str,phases ph,stratizen_types strtype');
    $this->db->where('str.stratizen_type_id=strtype.stratizen_type_id');
    $this->db->where('str.stratizen_type_id',2);
    $this->db->order_by('str.stratizen_fname');
    $this->db->order_by('str.stratizen_lname');
    $this->db->group_by('str.stratizen_su_id');
    $result=$this->db->get()->result_array();
    return $result;
}
//Staff registration
public function newStaff($stratizen_details)
{
    if($this->db->insert('stratizens',$stratizen_details))
        {
            return true;
        }
         else
            {
                return false;
            }
}
//Upcoming Conferences
public function upcomingConf()
{
    $this->db->select('*');
    $this->db->from('conferences');
    $this->db->where('conf_date_from >',date("Y-m-d"));
    $this->db->where('conf_cancel',0);
    $result=$this->db->get()->result_array();
    return $result;
}
//Upcoming Conferences
public function ongoingConf()
{
    $this->db->select('*');
    $this->db->from('conferences');
    $this->db->where('conf_date_from <=',date("Y-m-d"));
    $this->db->where('conf_date_to >=',date("Y-m-d"));
    $this->db->where('conf_cancel',0);
    $result=$this->db->get()->result_array();
    return $result;
}
//Closed Conferences
public function closedConf()
{
    $this->db->select('*');
    $this->db->from('conferences');
    $this->db->where('conf_date_to <',date("Y-m-d"));
    $this->db->where('conf_cancel',0);
    $result=$this->db->get()->result_array();
    return $result;
}
//conference registration
public function newConference($confDetails)
{
    if($this->db->insert('conferences',$confDetails))
        {
            return true;
        }
         else
        {
            return false;
        }
}
}