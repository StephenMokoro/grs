<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* 
*/
class LoginModel extends CI_Model
{
	function __construct()
		{
		     parent::__construct();
		     $this->load->database();
		}

    //login admin
	public function validateAdmin($username, $password) 
        {
            $this->db->select('*');
            $this->db->from('admin');
            $this->db->where('admin_username', $username);
            $this->db->where('admin_password', $password);
            $this->db->where('admin_active_status', 1);
            $this->db->limit(1);
            $query = $this->db->get();
             if ($query->num_rows() == 1) 
                {
                    return $query->result(); 
                } else 
                        {
                            return false;
                        }
        }
    //login security guard
    public function validateGuard($username, $password) 
        {
            $this->db->select('*');
            $this->db->from('guards');
            $this->db->where('guard_username', $username);
            $this->db->where('guard_password', $password);
            $this->db->where('guard_active_status', 1);
            $this->db->limit(1);
            $query = $this->db->get();
             if ($query->num_rows() == 1) 
                {
                    return $query->result(); 
                } else 
                        {
                            return false;
                        }
    }
     public function validateHdStaff($username, $password) 
        {
            $this->db->select('*');
            $this->db->from('hd_staff');
            $this->db->where('hd_staff_username', $username);
            $this->db->where('hd_staff_password', $password);
            $this->db->where('hd_staff_active_status', 1);
            $this->db->limit(1);
            $query = $this->db->get();
             if ($query->num_rows() == 1) 
                {
                    return $query->result(); 
                } else 
                        {
                            return false;
                        }
    }

}