<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @name LoginCtrl.php
 * @Mokoro
 */

//controller to login users
class Lg extends CI_Controller
{
    //constructor to initialize variables and load tools
    function __construct() 

    {
        parent::__construct();
        $this->load->model("LoginModel", "login");
        // $this->load->model("MainModel", "mainmodel");
    }
public function login() 
    {
    	//get submitted credentials
    	$username=$this->input->post('username',TRUE);
        $password=md5($this->input->post('password',TRUE));

        //try login 
        $admin = $this->login->validateAdmin($username, $password);//admin
        $guard = $this->login->validateGuard($username, $password);//security guard
        $hdstaff = $this->login->validateHdStaff($username, $password);//Helpdesk Staff
        
			if($admin) 
                {
                    //take the returned data and create a session for it (adminName and adminID). 
                    foreach ($admin as $row)
                            { 
                                $fullName=$row->admin_fname."&nbsp;".$row->admin_lname;
                                $userID=$row->staff_id;
                                $sessdata=array();
                                $sessdata = array('adminName' =>$fullName,'adminID'=>$userID,'adminLogin'=>TRUE); 
                                $this->session->set_userdata($sessdata);
                                        
                            }
                            redirect('MC/guardsView');
                                
       		} else if($guard)
                    {
                        foreach ($guard as $row)
                                { 
                                    $fullName=$row->guard_fname."&nbsp;".$row->officer_lname;
                                    $userID=$row->guard_staff_id;
                                    $sessdata=array();
                                    $sessdata = array('guardName' =>$fullName,'guardID'=>$userID,'guardLogin'=>TRUE); 
                                    $this->session->set_userdata($sessdata);
                                }
                                redirect('MC/dashboard');
                    }else if($hdstaff)
                        {
                            foreach ($officer as $row)
                                    { 
                                        $hdFullName=$row->hd_staff_fname."&nbsp;".$row->hd_staff_lname;
                                        $hdUserID=$row->hd_staff_su_id;
                                        $sessdata=array();
                                        $sessdata = array('hdStaffName' =>$hdFullName,'hdStaffID'=>$hdUserID,'hdStaffLogin'=>TRUE); 
                                        $this->session->set_userdata($sessdata);
                                    }
                                    redirect('MC/dash_hd');
                        } else
                            {
                                
                                $feedback = array('error' => "Wrong credentials. Please try again");
                                $this->session->set_flashdata('msg',$feedback);
                                redirect(base_url('MC'));
        	                }
    }

}



