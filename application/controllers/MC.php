<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* 
*/
class MC extends CI_Controller
{

function __construct()
{
     parent::__construct();
     $this->load->model('MainModel','mm');
}

public function index()
{
	$this->load->view('homepage');
}
//load admin page 
public function adminsView()
{
	$admin_list['admins']=$this->mm->activeAdminsList();
	$this->load->view('admin/admin_registration',$admin_list);
}

//admin profile
public function adminMore()
{
	$staffId=$this->input->post('staffId', TRUE);
	$admin_profile['admin_profile']=$this->mm->adminProfile($staffId);
	$this->load->view('admin/admin_profile',$admin_profile);
}
//admin Profile Update
public function editAdmin()
{
	$staffId=$this->input->post('staffId', TRUE);
	$admin_profile['admin_profile']=$this->mm->adminProfile($staffId);
	$this->load->view('admin/edit_admin',$admin_profile);
}
//admin registration 
public function newAdmin()
{
	//admin registration
	$staff_id=$this->input->post('staffID', TRUE);
	$first_name=$this->input->post('firstName', TRUE);
	$last_name=$this->input->post('lastName', TRUE);
	$other_names=$this->input->post('otherNames', TRUE);
	$admin_nid=$this->input->post('nationalID', TRUE);
	$phone_no=$this->input->post('phoneNumber', TRUE);
	$email=$this->input->post('emailAddress', TRUE);
	$admin_username=$this->input->post('userName', TRUE);
	$password=md5($admin_username.$staff_id);//default password
	$dateRegistered= date("Y-m-d"); 
	//create an array of the data to be inserted at once
	$admin_details = array('admin_staff_id' => $staff_id, 'admin_fname'=>$first_name, 'admin_lname'=>$last_name, 'admin_other_names'=>$other_names, 'admin_nid'=>$admin_nid,'admin_phone'=>$phone_no,'admin_email'=>$email, 'admin_username'=>$admin_username,'date_registered'=>$dateRegistered,'password'=>$password);
	
	$this->db->select('*');
	$this->db->from('admin');
	$this->db->where('admin_nid',$admin_nid);
	$this->db->or_where('admin_staff_id',$staff_id);
	$query = $this->db->get();
    $num=$query->num_rows(); 
    if($num>0)
        {
        	$feedback = array('error' => "Duplicate National/Staff ID/Username",'success' => "");
			$this->session->set_flashdata('msg',$feedback);
            redirect(base_url(('MC/adminsView')));
        }else 
            {
            	$this->db->select('*');
				$this->db->from('admin');
				$this->db->where('admin_username',$admin_username);
				$query = $this->db->get();
	            $num=$query->num_rows(); 
	            if($num>0)
		            {
		            	$feedback = array('error' => "Duplicate username",'success' => "");
						$this->session->set_flashdata('msg',$feedback);
			            redirect(base_url(('MC/adminsView')));
		            }else
		            	{
					            	$result=$this->mm->newAdmin($admin_details);
									if($result)
										{
											$feedback = array('error' => "",'success' => "New admin added");
											$this->session->set_flashdata('msg',$feedback);
						                   redirect(base_url(('MC/adminsView')));
										}else 
											{
												$feedback = array('error' => "Registration failed",'success' => "");
												$this->session->set_flashdata('msg',$feedback);
							                   redirect(base_url(('MC/adminsView')));
											}
            			}
           }

}
//update admin
public function updateAdmin()
{
	$admin_nid=$this->input->post('nationalID', TRUE);
	$staff_id=$this->input->post('staffID', TRUE);
	$first_name=$this->input->post('firstName', TRUE);
	$last_name=$this->input->post('lastName', TRUE);
	$other_names=$this->input->post('otherNames', TRUE);
	$phone_no=$this->input->post('phoneNumber', TRUE);
	$email=$this->input->post('emailAddress', TRUE);
	$admin_username=$this->input->post('userName', TRUE);
	//create an array of the data to be inserted at once
	$admin_details = array('admin_staff_id' => $staff_id, 'admin_fname'=>$first_name, 'admin_lname'=>$last_name, 'admin_other_names'=>$other_names,'admin_phone'=>$phone_no,'admin_email'=>$email, 'admin_username'=>$admin_username);
	$this->db->select('admin_staff_id');
    $this->db->from('admin');
    $this->db->where('admin_staff_id',$staff_id);
    $result=$this->db->get_compiled_select();

    $this->db->select('admin_staff_id');
    $this->db->from('admin');
    $this->db->where("`admin_staff_id` NOT IN ($result)", NULL, FALSE);
	$this->db->where('admin_username ',$admin_username);
	$query = $this->db->get();
    $num=$query->num_rows(); 
    if($num>0)
        {
        	$feedback = array('error' => "Duplicate Username",'success' => "");
			$this->session->set_flashdata('msg',$feedback);
            redirect(base_url(('MC/adminsView')));
        }else{
        		$this->db->select('admin_staff_id');
			    $this->db->from('admin');
			    $this->db->where("`admin_staff_id` NOT IN ($result)", NULL, FALSE);
				$this->db->where('admin_username ',$admin_nid);
				$query = $this->db->get();
			    $num=$query->num_rows(); 
			    if($num>0)
			        {
			        	$feedback = array('error' => "Duplicate National ID",'success' => "");
						$this->session->set_flashdata('msg',$feedback);
			            redirect(base_url(('MC/adminsView')));
			        }else{
				        	$result=$this->mm->updateAdmin($admin_details,$staff_id);
							if($result)
								{
									$feedback = array('error' => "",'success' => "Updated");
									$this->session->set_flashdata('msg',$feedback);
						           redirect(base_url(('MC/adminsView')));
								}else 
									{
										$feedback = array('error' => "No Changes",'success' => "");
										$this->session->set_flashdata('msg',$feedback);
						               redirect(base_url(('MC/adminsView')));
									}
						}	

			}

}
//disable admin
public function disableAdmin()
{
	$staffId=$this->input->post('staffId', TRUE);
	$reasonToDisable=$this->input->post('reasonToDisable', TRUE);
	$updateDetails=array('active_status'=>0, 'reason_inactive'=>$reasonToDisable);
	$result=$this->mm->disableAdmin($updateDetails,$staffId);
	if($result)
		{
			$feedback = array('error' => "",'success' => "admin disabled");
			$this->session->set_flashdata('msg',$feedback);
           redirect(base_url(('MC/adminsView')));
		}else 
			{
				$feedback = array('error' => "Failed to disable",'success' => "");
				$this->session->set_flashdata('msg',$feedback);
               redirect(base_url(('MC/adminsView')));
			}
}
//load security guards page 
public function guardsView()
{
	$guards_list['guards']=$this->mm->activeGuardsList();
	$this->load->view('admin/guard_registration',$guards_list);
}
//officer profile
public function officerProfile()
{
	$staffId=$this->input->post('staffId', TRUE);
	$officer_profile['officer_profile']=$this->mm->officerProfile($staffId);
	$this->load->view('admin/officer_profile',$officer_profile);
}
//Security Guard Profile Update
public function editGuard()
{
	$staffId=$this->input->post('staffId', TRUE);
	$guard_profile['guard_profile']=$this->mm->guardProfile($staffId);
	$this->load->view('admin/edit_guard',$guard_profile);
}
//Secuiryt Guard Registration 
public function newGuard()
{
	//Officer registration
	$phaseId=$this->input->post('phaseId', TRUE);
	$staff_id=$this->input->post('staffID', TRUE);
	$first_name=$this->input->post('firstName', TRUE);
	$last_name=$this->input->post('lastName', TRUE);
	$other_names=$this->input->post('otherNames', TRUE);
	$guard_nid=$this->input->post('nationalID', TRUE);
	$phone_no=$this->input->post('phoneNumber', TRUE);
	$email=$this->input->post('emailAddress', TRUE);
	$guard_username=$this->input->post('userName', TRUE);
	$password=md5($guard_username.$staff_id);
	$dateRegistered= date("Y-m-d"); 
	//create an array of the data to be inserted at once
	$guard_details = array('guard_phase_id'=>$phaseId,'guard_staff_id' => $staff_id, 'guard_fname'=>$first_name, 'guard_lname'=>$last_name, 'guard_other_names'=>$other_names, 'guard_nid'=>$guard_nid,'guard_phone'=>$phone_no,'guard_email'=>$email, 'guard_username'=>$guard_username,'guard_date_registered'=>$dateRegistered,'guard_password'=>$password);
	
	$this->db->select('*');
	$this->db->from('guards');
	$this->db->where('guard_nid',$guard_nid);
	$this->db->or_where('guard_staff_id',$staff_id);
	$query = $this->db->get();
    $num=$query->num_rows(); 
    if($num>0)
        {
        	$feedback = array('error' => "Duplicate National/Staff ID",'success' => "");
			$this->session->set_flashdata('msg',$feedback);
            redirect(base_url(('MC/guardsView')));
        }else 
            {
            	$this->db->select('*');
				$this->db->from('guards');
				$this->db->where('guard_username',$guard_username);
				$query = $this->db->get();
	            $num=$query->num_rows(); 
	            if($num>0)
		            {
		            	$feedback = array('error' => "Duplicate username",'success' => "");
						$this->session->set_flashdata('msg',$feedback);
			            redirect(base_url(('MC/guardsView')));
		            }else
		            	{
					            	$result=$this->mm->newGuard($guard_details);
									if($result)
										{
											$feedback = array('error' => "",'success' => "New Secuiry Guard Added");
											$this->session->set_flashdata('msg',$feedback);
						                   redirect(base_url(('MC/guardsView')));
										}else 
											{
												$feedback = array('error' => "Registration failed",'success' => "");
												$this->session->set_flashdata('msg',$feedback);
							                   redirect(base_url(('MC/guardsView')));
											}
            			}
           }

}
//update guard
public function updateGuard()
{
	$phaseId=$this->input->post('phaseId', TRUE);
	$staffId=$this->input->post('staffID', TRUE);
	$guard_nid=$this->input->post('nationalID', TRUE);
	$first_name=$this->input->post('firstName', TRUE);
	$last_name=$this->input->post('lastName', TRUE);
	$other_names=$this->input->post('otherNames', TRUE);
	$phone_no=$this->input->post('phoneNumber', TRUE);
	$email=$this->input->post('emailAddress', TRUE);
	$guard_username=$this->input->post('userName', TRUE);
	//create an array of the data to be inserted at once
	$guard_details = array('guard_phase_id'=>$phaseId,'guard_nid' => $guard_nid, 'guard_fname'=>$first_name, 'guard_lname'=>$last_name, 'guard_other_names'=>$other_names,'guard_phone'=>$phone_no,'guard_email'=>$email, 'guard_username'=>$guard_username);

	$this->db->select('guard_staff_id');
    $this->db->from('guards');
    $this->db->where('guard_staff_id',$staffId);
    $result=$this->db->get_compiled_select();

    $this->db->select('guard_staff_id');
    $this->db->from('guards');
    $this->db->where("`guard_staff_id` NOT IN ($result)", NULL, FALSE);
	$this->db->where('guard_username',$guard_username);
	$query = $this->db->get();
    $num=$query->num_rows(); 
    if($num>0)
        {
        	$feedback = array('error' => "Duplicate Username",'success' => "");
			$this->session->set_flashdata('msg',$feedback);
            redirect(base_url(('MC/guardsView')));
        }else
        	{
        		$this->db->select('guard_staff_id');
			    $this->db->from('guards');
			    $this->db->where("`guard_staff_id` NOT IN ($result)", NULL, FALSE);
				$this->db->where('guard_nid',$guard_nid);
				$query = $this->db->get();
			    $num=$query->num_rows(); 
			     if($num>0)
			        {
			        	$feedback = array('error' => "Duplicate National ID",'success' => "");
						$this->session->set_flashdata('msg',$feedback);
			            redirect(base_url(('MC/guardsView')));
			        }else
        					{
								$result=$this->mm->updateGuard($guard_details,$staffId);
								if($result)
									{
										$feedback = array('error' => "",'success' => "Updated");
										$this->session->set_flashdata('msg',$feedback);
							           redirect(base_url(('MC/guardsView')));
									}else 
										{
											$feedback = array('error' => "No Changes",'success' => "");
											$this->session->set_flashdata('msg',$feedback);
							               redirect(base_url(('MC/guardsView')));
										}
							}
				}

}

//Disable  security guard 
public function disableGuard()
{
	$staffId=$this->input->post('staffId', TRUE);
	$reasonToDisable=$this->input->post('reasonToDisable', TRUE);
	$updateDetails=array('guard_active_status'=>0, 'guard_reason_inactive'=>$reasonToDisable);
	$result=$this->mm->disableGuard($updateDetails,$staffId);
	if($result)
		{
			$feedback = array('error' => "",'success' => "Security Guard disabled");
			$this->session->set_flashdata('msg',$feedback);
           redirect(base_url(('MC/guardsView')));
		}else 
			{
				$feedback = array('error' => "Failed to disable",'success' => "");
				$this->session->set_flashdata('msg',$feedback);
               redirect(base_url(('MC/guardsView')));
			}
}


//helpdesks view page (security guards)
public function helpdesksView()
{
	$list['floors']=$this->mm->floorsList();
	$list['buildings']=$this->mm->buildingsList();
	$list['helpdesks']=$this->mm->helpdesksList();
	$this->load->view('officer/hd_registration',$list);
}
//helpdesks view page (admin)
public function hdView()
{
	$list['floors']=$this->mm->floorsList();
	$list['buildings']=$this->mm->buildingsList();
	$list['helpdesks']=$this->mm->helpdesksList();
	$this->load->view('admin/helpdesks',$list);
}
//Security Officers Dashboard
public function dashboard()
{
	$list['guests']=$this->mm->quickGuestsList();
	$this->load->view('officer/fast_guest_registration',$list);
}
public function newHelpdesk()
{
	//Helpdesk registration
	$phaseId=$this->input->post('phaseId', TRUE);
	$helpdeskName=$this->input->post('helpdeskName', TRUE);
	$buildingId=$this->input->post('buildingId', TRUE);
	$floorNo=$this->input->post('floorNo', TRUE);
	$dateRegistered= date("Y-m-d"); 
	//create an array of the data to be inserted at once
	$helpdesk_details = array('hd_phase_id'=>$phaseId,'hd_name' => $helpdeskName,'hd_building_id'=>$buildingId, 'hd_floor_no'=>$floorNo,'date_registered'=>$dateRegistered);
	
	$this->db->select('*');
	$this->db->from('helpdesks');
	$this->db->where('hd_phase_id',$phaseId);
	$this->db->where('hd_name',$helpdeskName);
	$this->db->where('hd_floor_no',$floorNo);
	$query = $this->db->get();
    $num=$query->num_rows(); 
    if($num>0)
        {
        	$feedback = array('error' => "Duplicate Helpdesk",'success' => "");
			$this->session->set_flashdata('msg',$feedback);
            redirect(base_url(('MC/helpdesksView')));
        }else 
            {
            	
	        	$result=$this->mm->newHelpdesk($helpdesk_details);
				if($result)
					{
						$feedback = array('error' => "",'success' => "New helpdesk added");
						$this->session->set_flashdata('msg',$feedback);
	                   redirect(base_url(('MC/helpdesksView')));
					}else 
						{
							$feedback = array('error' => "Registration failed",'success' => "");
							$this->session->set_flashdata('msg',$feedback);
		                   redirect(base_url(('MC/helpdesksView')));
						}

           }

}
//helpdesk edit page
public function editHelpdesk()
{
	$hd_auto_id=$this->input->post('helpdeskId', TRUE);
	$list['floors']=$this->mm->floorsList();
	$list['buildings']=$this->mm->buildingsList();
	$list['helpdesks']=$this->mm->helpdesksList();
	$list['helpdesk']=$this->mm->getHelpdesk($hd_auto_id);
	$this->load->view('officer/edit_helpdesk',$list);
}

public function updateHelpdesk()
{
	//Helpdesk update
	$helpdeskId=$this->input->post('helpdeskId', TRUE);
	$phaseId=$this->input->post('phaseId', TRUE);
	$helpdeskName=$this->input->post('helpdeskName', TRUE);
	$buildingId=$this->input->post('buildingId', TRUE);
	$floorNo=$this->input->post('floorNo', TRUE);
	//create an array of the data to be inserted at once
	$helpdesk_details = array('hd_phase_id'=>$phaseId,'hd_name' => $helpdeskName,'hd_building_id'=>$buildingId, 'hd_floor_no'=>$floorNo);
            	
	$result=$this->mm->updateHelpdesk($helpdesk_details,$helpdeskId);
	if($result)
		{
			$feedback = array('error' => "",'success' => "Update successful");
			$this->session->set_flashdata('msg',$feedback);
           redirect(base_url(('MC/helpdesksView')));
		}else 
			{
				$feedback = array('error' => "No changes",'success' => "");
				$this->session->set_flashdata('msg',$feedback);
               redirect(base_url(('MC/helpdesksView')));
			}    

}

//load helpdesk staff page (security guards) 
public function hdStaffView()
{
	$list['hdstaff']=$this->mm->activeHdStaffList();
	$list['helpdesks']=$this->mm->helpdesksList();
	$this->load->view('officer/hd_officer_registration',$list);
}
//load helpdesk staff page (admin guards) 
public function hdStaffAdminView()
{
	$list['hdstaff']=$this->mm->activeHdStaffList();
	$list['helpdesks']=$this->mm->helpdesksList();
	$this->load->view('officer/hd_officer_registration',$list);
}
//Officer registration 
public function newHdOfficer()
{
	$helpdeskId=$this->input->post('helpdeskId', TRUE);
	$staff_id=$this->input->post('staffID', TRUE);
	$first_name=$this->input->post('firstName', TRUE);
	$last_name=$this->input->post('lastName', TRUE);
	$other_names=$this->input->post('otherNames', TRUE);
	$hd_officer_nid=$this->input->post('nationalID', TRUE);
	$phone_no=$this->input->post('phoneNumber', TRUE);
	$email=$this->input->post('emailAddress', TRUE);
	$hd_officer_username=$this->input->post('userName', TRUE);
	$password=md5($hd_officer_username.$staff_id);
	$dateRegistered= date("Y-m-d"); 
	
	//create an array of the data to be inserted at once
	$hd_officer_details = array('hd_officer_hd_auto_id'=>$helpdeskId,'hd_officer_staff_id' => $staff_id, 'hd_officer_fname'=>$first_name, 'hd_officer_lname'=>$last_name, 'hd_officer_other_names'=>$other_names, 'hd_officer_nid'=>$hd_officer_nid,'hd_officer_phone'=>$phone_no,'hd_officer_email'=>$email, 'hd_officer_username'=>$hd_officer_username,'date_registered'=>$dateRegistered,'password'=>$password);
	
	$this->db->select('*');
	$this->db->from('hd_officers');
	$this->db->where('hd_officer_nid',$hd_officer_nid);
	$this->db->or_where('hd_officer_staff_id',$staff_id);
	$this->db->or_where('hd_officer_username',$hd_officer_username);
	$query = $this->db->get();
    $num=$query->num_rows(); 
    if($num>0)
        {
        	$feedback = array('error' => "Duplicate National/Staff ID",'success' => "");
			$this->session->set_flashdata('msg',$feedback);
            redirect(base_url(('MC/hdOfficersView')));
        }else 
            {
            	$this->db->select('*');
				$this->db->from('hd_officers');
				$this->db->where('hd_officer_username',$hd_officer_username);
				$query = $this->db->get();
	            $num=$query->num_rows(); 
	            if($num>0)
		            {
		            	$feedback = array('error' => "Duplicate username",'success' => "");
						$this->session->set_flashdata('msg',$feedback);
			            redirect(base_url(('MC/hdOfficersView')));
		            }else
		            	{
					            	$result=$this->mm->newHdOfficer($hd_officer_details);
									if($result)
										{
											$feedback = array('error' => "",'success' => "New officer added");
											$this->session->set_flashdata('msg',$feedback);
						                   redirect(base_url(('MC/hdOfficersView')));
										}else 
											{
												$feedback = array('error' => "Registration failed",'success' => "");
												$this->session->set_flashdata('msg',$feedback);
							                   redirect(base_url(('MC/hdOfficersView')));
											}
            			}
           }

}

//update Helpdesk officer
public function updateHdOfficer()
{
	$helpdeskId=$this->input->post('helpdeskId', TRUE);
	$staffId=$this->input->post('staffID', TRUE);
	$hd_officer_nid=$this->input->post('nationalID', TRUE);
	$first_name=$this->input->post('firstName', TRUE);
	$last_name=$this->input->post('lastName', TRUE);
	$other_names=$this->input->post('otherNames', TRUE);
	$phone_no=$this->input->post('phoneNumber', TRUE);
	$email=$this->input->post('emailAddress', TRUE);
	$hd_officer_username=$this->input->post('userName', TRUE);
	//create an array of the data to be inserted at once
	$hd_officer_details = array('hd_officer_hd_auto_id'=>$helpdeskId,'hd_officer_nid' => $hd_officer_nid, 'hd_officer_fname'=>$first_name, 'hd_officer_lname'=>$last_name, 'hd_officer_other_names'=>$other_names,'hd_officer_phone'=>$phone_no,'hd_officer_email'=>$email, 'hd_officer_username'=>$hd_officer_username);

	$this->db->select('hd_officer_staff_id');
    $this->db->from('hd_officers');
    $this->db->where('hd_officer_staff_id',$staffId);
    $result=$this->db->get_compiled_select();

    $this->db->select('hd_officer_staff_id');
    $this->db->from('hd_officers');
    $this->db->where("`hd_officer_staff_id` NOT IN ($result)", NULL, FALSE);
	$this->db->where('hd_officer_username',$hd_officer_username);
	$query = $this->db->get();
    $num=$query->num_rows(); 
    if($num>0)
        {
        	$feedback = array('error' => "Duplicate Username",'success' => "");
			$this->session->set_flashdata('msg',$feedback);
            redirect(base_url(('MC/hdOfficersView')));
        }else
        	{
        		$this->db->select('hd_officer_staff_id');
			    $this->db->from('hd_officers');
			    $this->db->where("`hd_officer_staff_id` NOT IN ($result)", NULL, FALSE);
				$this->db->where('hd_officer_nid',$hd_officer_nid);
				$query = $this->db->get();
			    $num=$query->num_rows(); 
			     if($num>0)
			        {
			        	$feedback = array('error' => "Duplicate National ID",'success' => "");
						$this->session->set_flashdata('msg',$feedback);
			            redirect(base_url(('MC/hdOfficersView')));
			        }else
        					{
								$result=$this->mm->updateHdOfficer($hd_officer_details,$staffId);
								if($result)
									{
										$feedback = array('error' => "",'success' => "Updated");
										$this->session->set_flashdata('msg',$feedback);
							           redirect(base_url(('MC/hdOfficersView')));
									}else 
										{
											$feedback = array('error' => "No Changes",'success' => "");
											$this->session->set_flashdata('msg',$feedback);
							               redirect(base_url(('MC/hdOfficersView')));
										}
							}
				}

}
//Officer Helpdesk Profile Update
public function editHdOfficer()
{
	$staffId=$this->input->post('staffId', TRUE);
	$list['helpdesks']=$this->mm->helpdesksList();
	$list['hd_officer_profile']=$this->mm->hdOfficerProfile($staffId);
	$this->load->view('officer/edit_hd_officer',$list);
}
//Disable Helpdesk officer
public function disableHdOfficer()
{
	$staffId=$this->input->post('staffId', TRUE);
	$reasonToDisable=$this->input->post('reasonToDisable', TRUE);
	$updateDetails=array('hd_active_status'=>0, 'hd_reason_inactive'=>$reasonToDisable);
	$result=$this->mm->disableHdOfficer($updateDetails,$staffId);
	if($result)
		{
			$feedback = array('error' => "",'success' => "Officer disabled");
			$this->session->set_flashdata('msg',$feedback);
           redirect(base_url(('MC/hdOfficersView')));
		}else 
			{
				$feedback = array('error' => "Failed to disable",'success' => "");
				$this->session->set_flashdata('msg',$feedback);
               redirect(base_url(('MC/hdOfficersView')));
			}
}
//all guests view (security guards view)
public function guestsView()
{
	$list['guests']=$this->mm->guestsList();
	// var_dump($list);
	$this->load->view('officer/all_guests',$list);
}
//all guests view (admin view)
public function allGuests()
{
	$list['guests']=$this->mm->guestsList();
	// var_dump($list);
	$this->load->view('admin/all_guests',$list);
}
//today's guests (security guard view)
public function newGuests()
{
	$list['guests']=$this->mm->newGuestsList();
	$this->load->view('officer/guest_registration',$list);
}
//today's guests (admin view)
public function dailyGuests()
{
	$list['guests']=$this->mm->newGuestsList();
	$this->load->view('admin/daily_guests',$list);
}
//Guest registration 
public function newGuest()
{
	$first_name=$this->input->post('firstName', TRUE);
	$last_name=$this->input->post('lastName', TRUE);
	$other_names=$this->input->post('otherNames', TRUE);
	$idNo=$this->input->post('IdNo', TRUE);
	$idType=$this->input->post('idType', TRUE);
	$phoneNumber=$this->input->post('phoneNumber', TRUE);

	$dateRegistered= date("Y-m-d"); 
	
	//create an array of the data to be inserted at once
	$guest_details = array('guest_fname'=>$first_name, 'guest_lname'=>$last_name, 'guest_other_names'=>$other_names, 'guest_id'=>$idNo,'guest_id_type'=>$idType,'guest_phone'=>$phoneNumber,'date_registered'=>$dateRegistered);
	
	$this->db->select('*');
	$this->db->from('guests');
	$this->db->where('guest_id',$idNo);
	$this->db->or_where('guest_phone',$phoneNumber);
	$query = $this->db->get();
    $num=$query->num_rows(); 
    if($num>0)
        {
        	$feedback = array('error' => "Duplicate ID/Phone No",'success' => "",'state'=>array('fullName'=>"",'recordId'=>""));
			$this->session->set_flashdata('msg',$feedback);
            redirect(base_url(('MC/newGuests')));
        }else 
            {
            	$result=$this->mm->newGuest($guest_details);
				if($result)
					{
						$feedback = array('error' => "",'success' => "New guest added",'state'=>array('fullName'=>"",'recordId'=>""));
						$this->session->set_flashdata('msg',$feedback);
	                   redirect(base_url(('MC/newGuests')));
					}else 
						{
							$feedback = array('error' => "Registration failed",'success' => "",'state'=>array('fullName'=>"",'recordId'=>""));
							$this->session->set_flashdata('msg',$feedback);
		                   redirect(base_url(('MC/newGuests')));
						}
            			
           }

}
//Fast Guest Registration 
public function fnGuest()
{
	$idNo=$this->input->post('IdNo', TRUE);
	$idType="1";
	$phoneNumber=$this->input->post('phoneNumber', TRUE);

	$dateRegistered= date("Y-m-d"); 
	
	//create an array of the data to be inserted at once
	$guest_details = array('guest_id'=>$idNo,'guest_id_type'=>$idType,'guest_phone'=>$phoneNumber,'date_registered'=>$dateRegistered);
	
	$this->db->select('*');
	$this->db->from('guests');
	$this->db->where('guest_id',$idNo);
	$this->db->or_where('guest_phone',$phoneNumber);
	$query = $this->db->get();
    $num=$query->num_rows(); 
    if($num>0)
        {
        	$feedback = array('error' => "Duplicate ID/Phone No",'success' => "",'state'=>array('fullName'=>"",'recordId'=>""));
			$this->session->set_flashdata('msg',$feedback);
            redirect(base_url(('MC/dashboard')));
        }else 
            {
            	$result=$this->mm->newGuest($guest_details);
				if($result)
					{
						$feedback = array('error' => "",'success' => "New guest added",'state'=>array('fullName'=>"",'recordId'=>""));
						$this->session->set_flashdata('msg',$feedback);
	                   redirect(base_url(('MC/dashboard')));
					}else 
						{
							$feedback = array('error' => "Registration failed",'success' => "",'state'=>array('fullName'=>"",'recordId'=>""));
							$this->session->set_flashdata('msg',$feedback);
		                   redirect(base_url(('MC/dashboard')));
						}
            			
           }

}
//Update Guest Profile 
public function updateGuest()
{
	$guestId=$this->input->post('guestId', TRUE);
	$first_name=$this->input->post('firstName', TRUE);
	$last_name=$this->input->post('lastName', TRUE);
	$other_names=$this->input->post('otherNames', TRUE);
	$idNo=$this->input->post('IdNo', TRUE);
	$idType=$this->input->post('idType', TRUE);
	$phoneNumber=$this->input->post('phoneNumber', TRUE);	
	//create an array of the data to be inserted at once
	$guest_details = array('guest_fname'=>$first_name, 'guest_lname'=>$last_name, 'guest_other_names'=>$other_names, 'guest_id'=>$idNo,'guest_id_type'=>$idType,'guest_phone'=>$phoneNumber);

		$result=$this->mm->updateGuest($guest_details,$guestId);
		if($result)
			{
				$feedback = array('error' => "",'success' => "Guest updated",'state'=>array('fullName'=>"",'recordId'=>""));
				$this->session->set_flashdata('msg',$feedback);
	           redirect(base_url(('MC/guestsView')));
			}else 
				{
					$feedback = array('error' => "No changes".$guest_id ,'success' => "",'state'=>array('fullName'=>"",'recordId'=>""));
					$this->session->set_flashdata('msg',$feedback);
	               redirect(base_url(('MC/guestsView')));
				}

}
public function editGuest()
{
	$guestId=$this->input->post('guestId', TRUE);
	$list['guest']=$this->mm->getGuest($guestId);
	$this->load->view('officer/edit_guest',$list);
}
public function viewGuest()
{
	$guestId=$this->input->post('guestId', TRUE);
	$list['guest']=$this->mm->getGuest($guestId);
	$this->load->view('officer/guest_profile',$list);
}
//list of all permitted entries (security guard view)
public function allActivePermits()
{
	$list['guests']=$this->mm->allActivePermits();
	$this->load->view('officer/all_active_perm_entries',$list);
}
//list of all permitted entries (admin view)
public function allCurrentPermits()
{
	$list['guests']=$this->mm->allActivePermits();
	$this->load->view('admin/all_active_perm_entries',$list);
}
//list of daily permitted entries (security guard view)
public function todayActivePermits()
{
	$list['guests']=$this->mm->todayActivePermits();
	$this->load->view('officer/today_active_permits',$list);
}
//list of daily permitted entries (admin view)
public function leoActivePermits()
{
	$list['guests']=$this->mm->todayActivePermits();
	$this->load->view('admin/today_active_permits',$list);
}
//list of daily closed entry permits (security guard view)
public function todayClosedPermits()
{
	$list['guests']=$this->mm->todayClosedPermits();
	$this->load->view('officer/today_closed_permits',$list);
}
//list of daily closed entry permits (admin view)
public function leoClosedPermits()
{
	$list['guests']=$this->mm->todayClosedPermits();
	$this->load->view('admin/today_closed_permits',$list);
}
//list of all closed entry permits (admin view)
public function allPastPermits()
{
	$list['guests']=$this->mm->allClosedPermits();
	$this->load->view('admin/all_closed_permits',$list);
}
//load new permit page
public function addPermEntry()
{
	$guestId=$this->input->post('guestId', TRUE);
	$list['guests']=$this->mm->getGuest($guestId);
	$this->load->view('officer/new_perm_entry',$list);
}
//load new permit page
public function fastPermEntry()
{
	$guestId=$this->input->post('guestId', TRUE);
	$list['guests']=$this->mm->getGuest($guestId);
	$this->load->view('officer/fast_perm_entry',$list);
}
//save new permit
public function newPermit()
{
	$guestId=$this->input->post('guestId', TRUE);
	$visitType=$this->input->post('visitType', TRUE);
	$personToVisit=$this->input->post('personToVisit', TRUE);
	$officeToVisit=$this->input->post('officeToVisit', TRUE);
	$timeOut=$this->input->post('timeOut', TRUE);
	$cardNo=$this->input->post('cardNo', TRUE);
	$minors=$this->input->post('minorsNo', TRUE);
	
	$entryDate= date("Y-m-d");

	$currentTime=date("h:i:s");

	$permit_details=array('entry_guest_auto_id'=>$guestId,'entry_date'=>$entryDate,'entry_time'=>$currentTime,'entry_expected_time_out'=>$timeOut, 'entry_office_to_visit'=>$officeToVisit,'entry_card_no'=>$cardNo,'entry_stratizen_to_visit'=>$personToVisit,'entry_visit_type'=>$visitType,'no_of_minors'=>$minors);

	$result=$this->mm->newPermit($permit_details);
	if($result)
		{
			$feedback = array('error' => "",'success' => "Permit added",'state'=>array('fullName'=>"",'recordId'=>""));
			$this->session->set_flashdata('msg',$feedback);
           redirect(base_url(('MC/todayActivePermits')));
		}else 
			{
				$feedback = array('error' => "Failed to add permit",'success' => "",'state'=>array('fullName'=>"",'recordId'=>""));
				$this->session->set_flashdata('msg',$feedback);
               redirect(base_url(('MC/guestsView')));
			}
            			
}
//error when permit for already exists
public function permError()
{
	$guestId=$this->input->post('guestId', TRUE);
	$entryRecordId=$this->input->post('permitRecordId', TRUE);
	$result=$this->mm->getGuest($guestId);
	
	$fullName;//to hold full name of guest
	foreach($result as $result)
	{
		$fullName=$result['guest_fname']." ".$result['guest_lname'];
	}
	$feedback = array('error' => "",'success' => "",'state'=>array('fullName'=>$fullName,'recordId'=>$entryRecordId));
	$this->session->set_flashdata('msg',$feedback);
	redirect(base_url(('MC/guestsView')));
}
//release guest
public function guestExit()
{
	$entryRecordId=$this->input->post('entryRecordId', TRUE);
	$exitDetails=array('entry_guest_release'=>1,'entry_actual_time_out'=>date("h:i:s"));
	$result=$this->mm->guestRelease($exitDetails,$entryRecordId);
		if($result)
			{
				$feedback = array('error' => "",'success' => "Guest Released",'state'=>array('fullName'=>"",'recordId'=>""));
				$this->session->set_flashdata('msg',$feedback);
	           redirect(base_url(('MC/guestsView')));
			}else 
				{
					$feedback = array('error' =>"Could not release guest",'success' => "",'state'=>array('fullName'=>"",'recordId'=>""));
					$this->session->set_flashdata('msg',$feedback);
	               redirect(base_url(('MC/guestsView')));
				}
}
public function getStratizen()
{
		$keyword=$this->input->get("q");
		$json = [];
		if(!empty($this->input->get("q"))){
			$this->db->or_like(array('stratizen_fname' => $keyword, 'stratizen_lname' => $keyword, 'stratizen_other_names' => $keyword));
			$query = $this->db->select('stratizen_auto_id as id,CONCAT(stratizen_su_id,": ",stratizen_fname," ",stratizen_lname) as text')
						->limit(10)
						->get("stratizens");
			$json = $query->result();
		}
		echo json_encode($json);
	
}
//get office for autocomplete 
public function getOffice()
{
		$keyword=$this->input->get("q");
		$json = [];
		if(!empty($this->input->get("q"))){
			$this->db->or_like(array('entry_office_to_visit' => $keyword));
			$this->db->group_by('entry_office_to_visit');
			$query = $this->db->select('entry_office_to_visit as id,entry_office_to_visit as text')
						->limit(10)
						->get("entry_permits");
			$json = $query->result();
		}
		echo json_encode($json);
	
}
//autocomplete staffroom
public function getStaffroom()
{
		$keyword=$this->input->get("q");
		$json = [];
		if(!empty($this->input->get("q"))){
			$this->db->or_like(array('stratizen_staffroom' => $keyword));
			$this->db->group_by('stratizen_staffroom');

			$query = $this->db->select('stratizen_staffroom as id,stratizen_staffroom as text')
						->limit(10)
						->get("stratizens");
			$json = $query->result();
		}
		echo json_encode($json);
	
}
//dashboard for helpdesk officer
public function dash_hd()
{
	$list['stratizens']=$this->mm->stratizens();
	$this->load->view('hdofficer/dashboard',$list);
}

//dashboard for helpdesk officer
public function staffList()
{
	$list['staffs']=$this->mm->allStaff();
	$this->load->view('hdofficer/staff_list',$list);}
public function addStaff()
{
	$list['staffs']=$this->mm->allStaff();
	$this->load->view('hdofficer/staff_registration',$list);
}
public function newStaff()
{
	//New Staff Registration
	$stratzCategory=$this->input->post('stratzCategory', TRUE);
	$staffId=$this->input->post('staffID', TRUE);
	$extension=$this->input->post('extension', TRUE);
	$first_name=$this->input->post('firstName', TRUE);
	$last_name=$this->input->post('lastName', TRUE);
	$other_names=$this->input->post('otherNames', TRUE);
	$phone_no=$this->input->post('phoneNumber', TRUE);
	$email=$this->input->post('emailAddress', TRUE);
	$staffRoom=$this->input->post('staffRoom', TRUE);

	$dateRegistered= date("Y-m-d"); 
	//create an array of the data to be inserted at once
	$stratizen_details = array('stratizen_type_id'=>$stratzCategory,'stratizen_su_id' => $staffId, 'stratizen_extension'=>$extension, 'stratizen_fname'=>$first_name, 'stratizen_lname'=>$last_name, 'stratizen_other_names'=>$other_names,'stratizen_phone'=>$phone_no,'stratizen_email'=>$email, 'stratizen_staffroom'=>$staffRoom,'date_registered'=>$dateRegistered);
	
	$this->db->select('*');
	$this->db->from('stratizens');
	$this->db->where('stratizen_su_id',$staffId);
	$query = $this->db->get();
    $num=$query->num_rows(); 
    if($num>0)
        {
        	$feedback = array('error' => "Duplicate Staff ID",'success' => "",'state'=>array('fullName'=>"",'recordId'=>""));
			$this->session->set_flashdata('msg',$feedback);
            redirect(base_url(('MC/staffList')));
        }else 
            {
            	
            	$result=$this->mm->newStaff($stratizen_details);
				if($result)
					{
						$feedback = array('error' => "",'success' => "New staff added",'state'=>array('fullName'=>"",'recordId'=>""));
						$this->session->set_flashdata('msg',$feedback);
	                   redirect(base_url(('MC/staffList')));
					}else 
						{
							$feedback = array('error' => "Registration failed",'success' => "",'state'=>array('fullName'=>"",'recordId'=>""));
							$this->session->set_flashdata('msg',$feedback);
		                   redirect(base_url(('MC/staffList')));
						}
           }

}
//register conference
public function newConference()
{
	$confName=$this->input->post('confName', TRUE);
	$confVenue=$this->input->post('confVenue', TRUE);
	$confStartDate=$this->input->post('confStartDate', TRUE);
	$confEndDate=$this->input->post('confEndDate', TRUE);
	$confDescription=$this->input->post('confDescription', TRUE);

	$confDetails=array('conf_name'=>$confName,'conf_venue'=>$confVenue,'conf_date_from'=>$confStartDate,'conf_date_to'=>$confEndDate,'conf_description'=>$confDescription);

	$this->db->select('*');
	$this->db->from('conferences');
	$this->db->where('conf_name',$confName);
	$this->db->where('conf_date_from',$confStartDate);
	$this->db->where('conf_date_to',$confEndDate);
	$query = $this->db->get();
    $num=$query->num_rows(); 
    if($num>0)
        {
        	$feedback = array('error' => "Duplicate Conference",'success' => "",'state'=>array('confName'=>"",'recordId'=>""));
			$this->session->set_flashdata('msg',$feedback);
            redirect(base_url(('MC/upcomingConf')));
        }else 
            {
            	
            	$result=$this->mm->newConference($confDetails);
				if($result)
					{
						$feedback = array('error' => "",'success' => "New conference added",'state'=>array('confName'=>"",'recordId'=>""));
						$this->session->set_flashdata('msg',$feedback);
	                   redirect(base_url(('MC/upcomingConf')));
					}else 
						{
							$feedback = array('error' => "Conference not added",'success' => "",'state'=>array('fullName'=>"",'recordId'=>""));
							$this->session->set_flashdata('msg',$feedback);
		                   redirect(base_url(('MC/upcomingConf')));
						}
           }



}
//Upcoming conferences
public function upcomingConf()
{
	$list['conferences']=$this->mm->upcomingConf();
	// var_dump($list);
	$this->load->view('officer/upcoming_conf',$list);
}

//Ongoing conferences
public function ongoingConf()
{
	$list['conferences']=$this->mm->ongoingConf();
	// var_dump($list);
	$this->load->view('officer/ongoing_conferences',$list);
}
//Closed conferences
public function closedConf()
{
	$list['conferences']=$this->mm->closedConf();
	// var_dump($list);
	$this->load->view('officer/closed_conferences',$list);
}
public function editConf()
{
	$staffId=$this->input->post('staffId', TRUE);
	$admin_profile['admin_profile']=$this->mm->adminProfile($staffId);
	$this->load->view('admin/edit_admin',$admin_profile);
}
}
